import 'package:flutter/material.dart';
import 'package:production_cost/feature/machianry/data/model/other_cost_response.dart';
import 'package:production_cost/feature/machianry/data/service/machine_service.dart';
import 'package:production_cost/feature/machianry/presentation/machine_add_iten.dart';
import 'package:production_cost/feature/machianry/presentation/machine_iten_data.dart';
import 'package:production_cost/shared/presentation/pages/menu_layout.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';
import 'package:provider/provider.dart';


import '../../../shared/presentation/widgets/card_iten.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import '../../../shared/utils/input_masks.dart';
class MachineMenuPage extends StatefulWidget {
  const MachineMenuPage({Key? key}) : super(key: key);

  @override
  State<MachineMenuPage> createState() => _MachineMenuPageState();
}

class _MachineMenuPageState extends State<MachineMenuPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<MachinaryProvider>(builder: (context, macState, child) {
        return Consumer<AuthProvider>(builder: (context, authState, child) {
          return MenuLayout(
            pageTitle: 'Maquinário',
              children: [
            Container(child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: macState.loadedMachines.length,
                itemBuilder: (context, index) {
                  return CardIten(
                    onTap: () {
                      openItenDesc(appState,authState, macState, index, context);
                    },
                    itenTitle: macState.loadedMachines[index].name!,
                    itenSubtitle: 'Data da Compra: ${dateUnmask(macState.loadedMachines[index].purchaseDate!)}',
                    itenDescription: 'Custo por hora: R\$${custPerHour(appState, macState,index)}',
                  );
                }),),
            SizedBox(height: 30,),
            Row(children: [Expanded(child: MyButton(
              label: 'Adicionar +',
              onPress: () {
                addActivity(appState,authState, macState, context);
              },
            ))
            ],)
          ]);
        });});});
  }

  void openItenDesc(AppProvider appState,AuthProvider authState, MachinaryProvider macState, int index, BuildContext context) {
    macState.machineIten = macState.loadedMachines[index];
    MachineService().searchMachinary(authState.token, '').then((value) {
      macState.machineTypeList = value;
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return MachineItenDataPage();
    }));
    });
  }

  void addActivity(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context) {
    macState.disposeControllers();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    macState.disposeControllers();
    MachineService().searchMachinary(authState.token, '').then((value) {
      macState.machineTypeList = value;
      macState.fuelTypeValue = macState.fuelTypeList.first;
      macState.otherCostList = [];
      macState.disposeControllers();
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return MachineAddIten();
      }));
    });
  }

  String custPerHour(AppProvider appState, MachinaryProvider macState, int index) {
    double fuelcost  = macState.loadedMachines[index].litersOfFuel! * macState.loadedMachines[index].fuelCapacityCost!;
    double utilitiesCost =  macState.loadedMachines[index].sumOfOilUsedCost!
        + macState.loadedMachines[index].literOfAdditivesCost!
        + macState.loadedMachines[index].sumOfFilterUsedCost!
    + macState.loadedMachines[index].greaseCost!;

    double otherCosts = 0;
    for(OtherCostResponse other in macState.loadedMachines[index].otherCost!){
      otherCosts = otherCosts+ other.value!;
    }
    double totalCost = fuelcost + utilitiesCost + otherCosts;
    return totalCost.toStringAsFixed(2);
  }
}

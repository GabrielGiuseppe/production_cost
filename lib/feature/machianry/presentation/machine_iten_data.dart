import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/machianry/data/service/machine_service.dart';
import 'package:production_cost/feature/machianry/presentation/machine_edit_iten.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_autocomplete_machine.dart';
import 'package:production_cost/shared/presentation/widgets/my_information_field.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';
import 'package:production_cost/shared/utils/input_masks.dart';
import 'package:provider/provider.dart';

import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import 'machine_menu_page.dart';

class MachineItenDataPage extends StatefulWidget {
  const MachineItenDataPage({Key? key}) : super(key: key);

  @override
  State<MachineItenDataPage> createState() => _MachineItenDataPageState();
}

class _MachineItenDataPageState extends State<MachineItenDataPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<MachinaryProvider>(builder: (context, macState, child) {
          return FormLayout(pageTitle: 'Maquinário', children: [
           MyInformationField(disabled: true,controller: TextEditingController(text: macState.machineIten.name),
              fieldTitle: 'Tipo de Maquinário',),
            MyInformationField(disabled: true,controller: TextEditingController(text: macState.machineIten.fuelType),
              fieldTitle: 'Tipo de Combustível',),
            MyInformationField(disabled: true,controller: TextEditingController(text: macState.machineIten.yearsOfUse.toString()),
            fieldTitle: 'Vida Util da Maquina (Anos)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: dateUnmask(macState.machineIten.purchaseDate!)),
              fieldTitle: 'Data da Compra',),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.machinePrice.toString()}'),
              fieldTitle: 'Valor da Máquina (R\$)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: macState.machineIten.hoursOfUsePerYear.toString()),
              fieldTitle: 'Horas de Uso por Ano',),
            Text('Custo R\$ por Hora de Uso', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
            const SizedBox(height: 25,),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.fuelCapacityCost.toString()}'),
              fieldTitle: 'Litro do Combustivel (R\$)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: macState.machineIten.litersOfFuel.toString()),
              fieldTitle: 'Quantidade, Litros Utilizados (Hora)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.sumOfOilUsedCost.toString()}'),
              fieldTitle: 'Soma dos Óleos utilizados (R\$)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.sumOfOilUsedCost.toString()}'),
              fieldTitle: 'Soma dos Filtros utilizados (R\$) (R\$)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.sumOfFilterUsedCost.toString()}'),
              fieldTitle: 'Soma dos Aditivos Utilizados (R\$)',),
            MyInformationField(disabled: true,controller: TextEditingController(text: 'R\$ ${macState.machineIten.greaseCost.toString()}'),
              fieldTitle: 'Graxa (R\$)',),
            Divider(thickness: 2,),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: macState.machineIten.otherCost!.length,
                itemBuilder: (context, index) {
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Iten: ${macState.machineIten.otherCost![index].description}',style: TextStyle(fontSize: 18, color: appState.darkMode?Colors.white:AppColor.font),),
                      Text('Custo: R\$ ${macState.machineIten.otherCost![index].value}', style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),),
                      const SizedBox(height: 15,),
                      Divider(thickness: 1,)
                    ],
                  ),
                );
          }),
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [MyButton(label: 'Voltar',
                onPress: (){
                  returnToMenu(appState,authState, macState, context);
                },
              ),],),
            const SizedBox(height: 25,),
            Row(children: [
              Expanded(child: MyButton(label: 'Editar Maquinário',
                onPress: (){
                  editFields(appState,authState,macState,context);
                },))
            ],),
            const SizedBox(height: 25,),
            Row(children: [
              Expanded(child: MyButton(label: 'Deletar Maquinário',backgroundColor: AppColor.redColor,
                onPress: (){
                  removeActivity(appState,authState, macState, context);
                },))
            ],),
          ]);
        });
      });
    });
  }

  void editFields(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context) {

    macState.machineTypeController = TextEditingController(text: macState.machineIten.name);
    macState.yearsOfUseController = TextEditingController(text: macState.machineIten.yearsOfUse.toString());
    macState.purchaseDateController = TextEditingController(text: dateUnmask(macState.machineIten.purchaseDate!));
    macState.machinePriceController = TextEditingController(text: macState.machineIten.machinePrice.toString());
    macState.litersOfFuelController = TextEditingController(text: macState.machineIten.litersOfFuel.toString());
    macState.hoursOfUsePerYearController = TextEditingController(text: macState.machineIten.hoursOfUsePerYear.toString());
    macState.fuelCapacityCostController = TextEditingController(text: macState.machineIten.fuelCapacityCost.toString());
    macState.sumOfOilUsedCostController = TextEditingController(text: macState.machineIten.sumOfOilUsedCost.toString());
    macState.sumOfFilterUsedCostController = TextEditingController(text: macState.machineIten.sumOfFilterUsedCost.toString());
    macState.literOfAdditivesCostController = TextEditingController(text: macState.machineIten.literOfAdditivesCost.toString());
    macState.greaseCostController = TextEditingController(text: macState.machineIten.greaseCost.toString());
    macState.otherCostList = macState.machineIten.otherCost!;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    MachineService().searchMachinary(authState.token, '').then((value) {
      macState.machineTypeList = value;
      macState.fuelTypeValue = macState.fuelTypeList.first;
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return MachineEditItemPage();
      }));
    });
  }

  void returnToMenu(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    MachineService().getAll(authState.token).then((value) {
      macState.loadedMachines = value;
      Navigator.pop(dialogContext);
      Navigator.pop(context);
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void removeActivity(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    MachineService().delete(macState.machineIten.id!, authState.token).then((value) {
      MachineService().getAll(authState.token).then((value) {
        macState.loadedMachines = value;
        Navigator.pop(dialogContext);
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return MachineMenuPage();
        }));
      });
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }
}

import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/machianry/data/model/machine_payload.dart';
import 'package:production_cost/feature/machianry/data/model/other_cost_response.dart';
import 'package:production_cost/feature/machianry/data/service/machine_service.dart';
import 'package:production_cost/feature/machianry/presentation/machine_menu_page.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_autocomplete_machine.dart';
import 'package:production_cost/shared/presentation/widgets/my_dropdown_field.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/presentation/widgets/my_date_picker_field.dart';
import '../../../shared/presentation/widgets/my_information_field.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import '../../../shared/providers/machinary_provider.dart';
import '../../../shared/utils/input_masks.dart';
import '../../../shared/utils/register_fild_validator.dart';
import '../../../shared/utils/string_utils.dart';

class MachineAddIten extends StatefulWidget {
  const MachineAddIten({Key? key}) : super(key: key);

  @override
  State<MachineAddIten> createState() => _MachineAddItenState();
}

class _MachineAddItenState extends State<MachineAddIten> {

  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<MachinaryProvider>(builder: (context, macState, child) {
        return Consumer<AuthProvider>(builder: (context, authState, child) {
          return FormLayout(pageTitle: 'Adicionar Maquinário', children: [
            MyAutocompleteMachineField(
              prefillText:
              macState.machineTypeController.text,
              fieldTitle: 'Tipo de Maquinário',
              onSubmitted: (String){
                FocusScope.of(context).requestFocus(macState.yearsOfUseFocusNode);
              },
            ),
            MyDropDownField(
              fieldType: FieldType.fuel,
              valuesList: macState.fuelTypeList,
              initialValue: macState.fuelTypeValue,
              fieldTitle: 'Tipo de Combustível',
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              controller:
              macState.yearsOfUseController,
              fieldTitle: 'Vida Util da Maquina (Anos)',
              focusNode: macState.yearsOfUseFocusNode,
              onSubmitted: (value) {
                macState.yearsOfUseFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.purchaseDateFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.yearsOfUse)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.yearsOfUse);
                });
              },
            ),
            MyDatePickerField(
                controller: macState.purchaseDateController,
                focusNode: macState.purchaseDateFocusNode,
                keyboardType: TextInputType.number,
                fieldTitle: 'Data da Compra',
                inputFormatters: [InputMasks.dateMask()],
                showAlert: appState.invalidFields.contains(ValidFields.purchaseDate)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.purchaseDate);
                  });
                },
                onPress: () async {
                  DateTime initialTime = DateTime.now();
                  if (macState.purchaseDateController.text.isNotEmpty) {
                    initialTime = StringUtils().convertStringToDate(macState.purchaseDateController.text, "dd/MM/yyyy");
                  }

                  DateTime? pickedDate = await showDatePicker(
                      context: context,
                      initialDate: initialTime,
                      firstDate: DateTime(1800),
                      lastDate: DateTime.now());

                  if (pickedDate != null) {
                    String formattedDate = DateFormat("dd/MM/yyyy").format(pickedDate);
                    setState(() {
                      macState.purchaseDateController.text = formattedDate;
                    });
                  }
                },
                onSubmitted: (value) {
                  macState.purchaseDateFocusNode.unfocus();
                  FocusScope.of(context).requestFocus(macState.machinePriceFocusNode);
                }
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.machinePriceController,
              fieldTitle: 'Valor da Máquina (R\$)',
              focusNode: macState.machinePriceFocusNode,
              onSubmitted: (value) {
                macState.machinePriceFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.hoursOfUsePerYearFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.machinePrice)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.machinePrice);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              controller:
              macState.hoursOfUsePerYearController,
              fieldTitle: 'Horas de Uso por Ano',
              focusNode: macState.hoursOfUsePerYearFocusNode,
              onSubmitted: (value) {
                macState.hoursOfUsePerYearFocusNode.unfocus();
              },
              showAlert: appState.invalidFields.contains(ValidFields.hoursOfUsePerYear)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.hoursOfUsePerYear);
                });
              },
            ),
            Text(
              'Custo R\$ por Hora de Uso',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            const SizedBox(
              height: 25,
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.litersOfFuelController,
              fieldTitle: 'Litro do Combustivel (R\$)',
              focusNode: macState.litersOfFuelFocusNode,
              onSubmitted: (value) {
                macState.litersOfFuelFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.fuelCapacityCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.litersOfFuel)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.litersOfFuel);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              controller:
              macState.fuelCapacityCostController,
              fieldTitle: 'Quantidade, Litros Utilizados (Hora)',
              focusNode: macState.fuelCapacityCostFocusNode,
              onSubmitted: (value) {
                macState.fuelCapacityCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.sumOfOilUsedCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.fuelCapacityCost)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.fuelCapacityCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.sumOfOilUsedCostController,
              fieldTitle: 'Soma dos Óleos utilizados (R\$)',
              focusNode: macState.sumOfOilUsedCostFocusNode,
              onSubmitted: (value) {
                macState.sumOfOilUsedCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.sumOfFilterUsedCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.sumOfOilUsedCost)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.sumOfOilUsedCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.sumOfFilterUsedCostController,
              fieldTitle: 'Soma dos Filtros utilizados (R\$)',
              focusNode: macState.sumOfFilterUsedCostFocusNode,
              onSubmitted: (value) {
                macState.sumOfFilterUsedCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.literOfAdditivesCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.sumOfFilterUsedCost)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.sumOfFilterUsedCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.literOfAdditivesCostController,
              fieldTitle: 'Soma dos Aditivos Utilizados (R\$)',
              focusNode: macState.literOfAdditivesCostFocusNode,
              onSubmitted: (value) {
                macState.literOfAdditivesCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.greaseCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.literOfAdditivesCost)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.literOfAdditivesCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.greaseCostController,
              fieldTitle: 'Graxa (R\$)',
              focusNode: macState.greaseCostFocusNode,
              onSubmitted: (value) {
                macState.greaseCostFocusNode.unfocus();
              },
              showAlert: appState.invalidFields.contains(ValidFields.greaseCost)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.greaseCost);
                });
              },
            ),

            Text('Informe abaixo o gasto com outros iten (R\$) por hora de uso', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
            const SizedBox(height: 5,),
            const Divider(
              thickness: 2,
            ),
            MyInformationField(
                controller: macState.descController,
              fieldTitle: 'Descrição do Item',
              focusNode: macState.descTypeFocusNode,
              onSubmitted: (value) {
                macState.descTypeFocusNode.unfocus();
                FocusScope.of(context).requestFocus(macState.valueFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.otherCostDesc)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.otherCostDesc);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [CurrencyTextInputFormatter(
                  decimalDigits: 2,
                  symbol: 'R\$'
              )],
              controller:
              macState.valueController,
              fieldTitle: 'Custo (R\$)',
              focusNode: macState.valueFocusNode,
              onSubmitted: (value) {
                macState.valueFocusNode.unfocus();
              },
              showAlert: appState.invalidFields.contains(ValidFields.otherCostValue)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.otherCostValue);
                });
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  width: 200,
                  label: 'Adicionar Item +',
                  onPress: () {
                    addItemCost(appState, authState, macState, context);
                  },
                ),
              ],
            ),
            const SizedBox(height: 15,),
            const Divider(
              thickness: 2,
            ),
            const SizedBox(height: 15,),
            macState.otherCostList.isEmpty?const SizedBox():
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: macState.otherCostList.length,
                    itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    Text('Item: ${macState.otherCostList[index].description}', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18, color: appState.darkMode?Colors.white:AppColor.font),),
                        const SizedBox(height: 10,),
                        Text('Custo: R\$ ${macState.otherCostList[index].value}', style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),),
                    const SizedBox(height: 15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        MyButton(
                          height: 35,
                          label: 'Remover -',
                          backgroundColor: AppColor.redColor,
                          onPress: () {
                            removeOtherCost(appState, authState, macState, context, index);
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 15,),
                    const Divider(
                      thickness: 2,
                    ),
                    const SizedBox(height: 15,),

                  ]);
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  label: 'Salvar',
                  onPress: () {
                    saveMachinary(appState, authState, macState, context);
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            const SizedBox(
              height: 25,
            ),
          ]);
        });
      });
    });
  }

  void saveMachinary(AppProvider appState, AuthProvider authState,
      MachinaryProvider macState, BuildContext context) {
    appState.invalidFields = [];
    MachinePayload payload = MachinePayload();
    checkFieldMachinary(ValidFields.machineName, appState, macState, macState.machineTypeController);
    checkFieldMachinary(ValidFields.yearsOfUse, appState, macState, macState.yearsOfUseController);
    checkFieldMachinary(ValidFields.purchaseDate, appState, macState, macState.purchaseDateController);
    checkFieldMachinary(ValidFields.machinePrice, appState, macState, macState.machinePriceController);
    checkFieldMachinary(ValidFields.hoursOfUsePerYear, appState, macState, macState.hoursOfUsePerYearController);
    checkFieldMachinary(ValidFields.litersOfFuel, appState, macState, macState.litersOfFuelController);
    checkFieldMachinary(ValidFields.fuelCapacityCost, appState, macState, macState.fuelCapacityCostController);
    checkFieldMachinary(ValidFields.sumOfOilUsedCost, appState, macState, macState.sumOfOilUsedCostController);
    checkFieldMachinary(ValidFields.sumOfFilterUsedCost, appState, macState, macState.sumOfFilterUsedCostController);
    checkFieldMachinary(ValidFields.literOfAdditivesCost, appState, macState, macState.literOfAdditivesCostController);
    checkFieldMachinary(ValidFields.greaseCost, appState, macState, macState.greaseCostController);

    setState(() {

    });
    if(appState.invalidFields.isNotEmpty){
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }else{
      payload = MachinePayload(
        name:macState.machineTypeController.text,
        yearsOfUse:int.parse(macState.yearsOfUseController.text),
        purchaseDate: dateMask(macState.purchaseDateController.text),
        machinePrice: double.parse(macState.machinePriceController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        hoursOfUsePerYear:int.parse(macState.hoursOfUsePerYearController.text),
        fuelType:macState.fuelTypeValue,
        litersOfFuel:double.parse(macState.litersOfFuelController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        fuelCapacityCost:double.parse(macState.fuelCapacityCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        hourlyCost:0,
        sumOfOilUsedCost:double.parse(macState.sumOfOilUsedCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        sumOfFilterUsedCost:double.parse(macState.sumOfFilterUsedCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        literOfAdditivesCost:double.parse(macState.literOfAdditivesCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        greaseCost:double.parse(macState.greaseCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        otherCost:macState.otherCostList,
      );
      showDialog(
          context: context,
          builder: (BuildContext context) {
            dialogContext = context;
            return const DefaultAlert(
              text: "Salvando...",
            );
          },
          useSafeArea: true,
          barrierDismissible: false);
      MachineService().add(payload, authState.token).then((value) {
        MachineService().getAll(authState.token).then((value) {
          macState.loadedMachines = value;
          Navigator.pop(dialogContext);
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return MachineMenuPage();
          }));
        });
      }).onError((error, stackTrace) {
        Navigator.pop(dialogContext);
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }

  void addItemCost(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context) {
    appState.invalidFields = [];
    checkFieldMachinary(ValidFields.otherCostDesc, appState, macState, macState.descController);
    checkFieldMachinary(ValidFields.otherCostValue, appState, macState, macState.valueController);
    setState(() {

    });
    if(macState.invalidFields.isEmpty) {
      OtherCostResponse otherCost = OtherCostResponse(
          description: macState.descController.text,
          value: double.parse(macState.valueController.text.replaceAll(RegExp(r'[^\d.]+'),'')));
      macState.addOtherCost(otherCost);
      macState.descController.text = '';
      macState.valueController.text = '';
    }else{

    }
  }

  void removeOtherCost(AppProvider appState, AuthProvider authState, MachinaryProvider macState, BuildContext context, int index) {
    macState.removeOtherCust(macState.otherCostList[index]);
    setState(() {

    });
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

import '../../../../shared/utils/app_strings.dart';
import '../model/machine_payload.dart';
import '../model/machine_response.dart';

class MachineService {
  String host = AppStrings.host;

  Future<dynamic> add(MachinePayload payload, String token) async {
    final response = await http.post(
        Uri.parse(
            '$host/machineries'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));

    print(jsonEncode(payload));
    if (response.statusCode == 201) {
      return MachineResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> edit(MachineResponse payload, String token) async {
    var sent = jsonDecode(jsonEncode(payload));
    final response = await http.put(
        Uri.parse(
            '$host/machineries'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));

    if (response.statusCode == 200) {
      return MachineResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> delete(String id, String token) async {
    final response = await http.delete(
        Uri.parse(
            '$host/machineries/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      return "success";
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<MachineResponse> get(String id, String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/machineries/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      return MachineResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<List<MachineResponse>> getAll(String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/machineries'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      List<MachineResponse> listProductive = (json.decode(response.body) as List)
          .map((data) => MachineResponse.fromJson(data))
          .toList();
      return listProductive;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<List<String>> searchMachinary(String token, String keyword) async {
    final response = await http.get(
        Uri.parse(
            '$host/machineries/search?keyword=$keyword'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    print(json.decode(response.body));
    List<String> listMachineType = [];
    if (response.statusCode == 200) {
      var listmachinary = json.decode(response.body);
      for (String machine in listmachinary) {
        listMachineType.add(machine);
      }
      return listMachineType;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  static const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  final Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}

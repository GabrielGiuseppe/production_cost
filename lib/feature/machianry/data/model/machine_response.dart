import 'dart:convert';

import 'other_cost_response.dart';

class MachineResponse{
  String? id;
  String? name;
  double? yearsOfUse;
  String? purchaseDate;
  double? machinePrice;
  double? hoursOfUsePerYear;
  double? litersOfFuel;
  String? fuelType;
  double? fuelCapacityCost;
  double? hourlyCost;
  double? sumOfOilUsedCost;
  double? sumOfFilterUsedCost;
  double? literOfAdditivesCost;
  double? greaseCost;
  List<OtherCostResponse>? otherCost;

  MachineResponse({
     this.id,
     this.name,
     this.yearsOfUse,
     this.purchaseDate,
     this.machinePrice,
     this.hoursOfUsePerYear,
     this.litersOfFuel,
     this.fuelType,
     this.fuelCapacityCost,
     this.hourlyCost,
     this.sumOfOilUsedCost,
     this.sumOfFilterUsedCost,
     this.literOfAdditivesCost,
     this.greaseCost,
     this.otherCost,
  });

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "yearsOfUse": yearsOfUse,
      "purchaseDate": purchaseDate,
      "machinePrice": machinePrice,
      "hoursOfUsePerYear": hoursOfUsePerYear,
      "litersOfFuel": litersOfFuel,
      "fuelType": fuelType,
      "fuelCapacityCost": fuelCapacityCost,
      "hourlyCost": hourlyCost,
      "sumOfOilUsedCost": sumOfOilUsedCost,
      "sumOfFilterUsedCost": sumOfFilterUsedCost,
      "literOfAdditivesCost": literOfAdditivesCost,
      "greaseCost": greaseCost,
      "otherCost": otherCost,
    };
  }

  factory MachineResponse.fromJson(Map<String, dynamic> json) {
    var list = json['otherCost'] as List;
    List<OtherCostResponse> otherCost = list.map((i) => OtherCostResponse.fromJson(i)).toList();
    return MachineResponse(
        id: json['id'],
        name: json['name'],
        yearsOfUse: double.parse(json['yearsOfUse'].toString()),
        purchaseDate: json['purchaseDate'],
        machinePrice: double.parse(json['machinePrice'].toString()),
        hoursOfUsePerYear: double.parse(json['hoursOfUsePerYear'].toString()),
        fuelType: json['fuelType'],
        litersOfFuel: double.parse(json['litersOfFuel'].toString()),
        fuelCapacityCost: double.parse(json['fuelCapacityCost'].toString()),
        hourlyCost: double.parse(json['hourlyCost'].toString()),
        sumOfOilUsedCost: double.parse(json['sumOfOilUsedCost'].toString()),
        sumOfFilterUsedCost: double.parse(json['sumOfFilterUsedCost'].toString()),
        literOfAdditivesCost: double.parse(json['literOfAdditivesCost'].toString()),
        greaseCost: double.parse(json['greaseCost'].toString()),
        otherCost: otherCost
    );
  }
}



class OtherCostResponse{
  String? id;
  String? description;
  double? value;

  OtherCostResponse({
    this.id,
    this.description,
    this.value
  });

  Map<String, dynamic> toJson() {
    return {
      "description": description,
      "value": value
    };
  }

  factory OtherCostResponse.fromJson(Map<String, dynamic> json) {

    OtherCostResponse response = OtherCostResponse(
      id: json['id'],
      description: json['description'],
      value: json['value']
    );
    return response;
  }
}





import 'other_cost_response.dart';

class MachinePayload {
  String?name;
  int?yearsOfUse;
  String?purchaseDate;
  double?machinePrice;
  int?hoursOfUsePerYear;
  String?fuelType;
  double?litersOfFuel;
  double?fuelCapacityCost;
  double?hourlyCost;
  double?sumOfOilUsedCost;
  double?sumOfFilterUsedCost;
  double?literOfAdditivesCost;
  double?greaseCost;
  List<OtherCostResponse>?otherCost;

  MachinePayload({
    this.name,
    this.yearsOfUse,
    this.purchaseDate,
    this.machinePrice,
    this.hoursOfUsePerYear,
    this.fuelType,
    this.litersOfFuel,
    this.fuelCapacityCost,
    this.hourlyCost,
    this.sumOfOilUsedCost,
    this.sumOfFilterUsedCost,
    this.literOfAdditivesCost,
    this.greaseCost,
    this.otherCost,
  });

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "yearsOfUse": yearsOfUse,
      "purchaseDate": purchaseDate,
      "machinePrice": machinePrice,
      "hoursOfUsePerYear": hoursOfUsePerYear,
      "fuelType": fuelType,
      "litersOfFuel" : litersOfFuel,
      "fuelCapacityCost": fuelCapacityCost,
      "hourlyCost": hourlyCost,
      "sumOfOilUsedCost": sumOfOilUsedCost,
      "sumOfFilterUsedCost": sumOfFilterUsedCost,
      "literOfAdditivesCost": literOfAdditivesCost,
      "greaseCost": greaseCost,
      "otherCost": otherCost,
    };
  }
}

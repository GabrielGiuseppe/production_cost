import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/model/property.dart';
import 'package:production_cost/feature/labor/data/service/labor_service.dart';
import 'package:production_cost/feature/labor/presentation/labor_menu_page.dart';
import 'package:production_cost/feature/machianry/data/service/machine_service.dart';
import 'package:production_cost/feature/machianry/presentation/machine_menu_page.dart';
import 'package:production_cost/feature/product-activity/data/service/productive_activity_service.dart';
import 'package:production_cost/feature/product-activity/presentation/activity_menu_page.dart';
import 'package:production_cost/feature/property/data/service/property_service.dart';
import 'package:production_cost/feature/property/persentation/property_informairon.dart';
import 'package:production_cost/shared/presentation/pages/menu_layout.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:production_cost/shared/providers/labor_provider.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';
import 'package:production_cost/shared/providers/property_provider.dart';
import 'package:production_cost/shared/utils/input_masks.dart';
import 'package:provider/provider.dart';

import '../shared/presentation/widgets/default_alert.dart';
import '../shared/presentation/widgets/menu_card.dart';
import '../shared/providers/app_provider.dart';
import '../shared/providers/auth_provider.dart';
import 'auth/data/service/city_service.dart';
import 'labor/data/model/labor_response.dart';

class DashboradPage extends StatefulWidget {
  const DashboradPage({Key? key}) : super(key: key);

  @override
  State<DashboradPage> createState() => _DashboradPageState();
}

class _DashboradPageState extends State<DashboradPage> {
  late BuildContext dialogContext;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<PropertyProvider>(builder: (context, propState, child) {
          return Consumer<MachinaryProvider>(
              builder: (context, macState, child) {
            return Consumer<LaborProvider>(builder: (context, labState, child) {
              return Consumer<ActivityProvider>(
                  builder: (covariant, actState, child) {
                return MenuLayout(
                  pageTitle: 'Home',
                  children: [
                    Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Editar Propriedade',
                              image: appState.darkMode
                                  ? "assets/images/edit_property_dark.png"
                                  : "assets/images/edit_property.png",
                              onTap: () {
                                callPropertyEditor(
                                    appState, authState, propState, context);
                              },
                            )),
                        const SizedBox(
                          width: 15,
                        ),
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Atividade Produtiva',
                              image: appState.darkMode
                                  ? "assets/images/productive_activity_dark.png"
                                  : "assets/images/productive_activity.png",
                              onTap: () {
                                callProductActivity(
                                    appState, authState, actState, context);
                              },
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Maquinário',
                              image: appState.darkMode
                                  ? "assets/images/machinery_dark.png"
                                  : "assets/images/machinery.png",
                              onTap: () {
                                callMachinary(
                                    appState, authState, macState, context);
                              },
                            )),
                        const SizedBox(
                          width: 15,
                        ),
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Mão de Obra',
                              image: appState.darkMode
                                  ? "assets/images/labor_dark.png"
                                  : "assets/images/labor2.png",
                              onTap: () {
                                callLabor(appState, authState,labState, context);
                              },
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Custo de Produção',
                              image: appState.darkMode
                                  ? "assets/images/production_cost_dark.png"
                                  : "assets/images/production_cost.png",
                              onTap: () {
                                callCost(appState, authState, context);
                              },
                            )),
                        const SizedBox(
                          width: 15,
                        ),
                        Expanded(
                            flex: 1,
                            child: MenuCard(
                              cardText: 'Análise de Custo',
                              image: appState.darkMode
                                  ? "assets/images/cost_analysis_dark.png"
                                  : "assets/images/cost_analysis.png",
                              onTap: () {
                                callAnalisys(appState, authState, context);
                              },
                            ))
                      ],
                    )
                  ],
                );
              });
            });
          });
        });
      });
    });
  }

  void callPropertyEditor(AppProvider appState, AuthProvider authState,
      PropertyProvider propState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Buscando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    PropertyService().getProperty(authState).then((value) {
      fillFields(appState, propState, value, context);
      propState.property = value;
      CityService().getCities(appState.ufValue).then((value) {
        appState.cityList = value;
        Navigator.pop(dialogContext);
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return PropertyDataPage();
        }));
      }).onError((error, stackTrace) {
        Navigator.pop(dialogContext);
      });
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void callProductActivity(AppProvider appState, AuthProvider authState,
      ActivityProvider actState, BuildContext context) {
    appState.isOnHome = false;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    ProductiveActivityService().getAllProductive(authState.token).then((value) {
      actState.loadedActivities = value;
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ActivityMenuPage();
      }));
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void callMachinary(AppProvider appState, AuthProvider authState,
      MachinaryProvider macState, BuildContext context) {
    appState.isOnHome = false;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    MachineService().getAll(authState.token).then((value) {
      macState.loadedMachines = value;
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return MachineMenuPage();
      }));
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void callLabor(
      AppProvider appState, AuthProvider authState,LaborProvider labState, BuildContext context) {
    appState.isOnHome = false;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    labState.loadedItens = [];
    LaborService().getAll(authState.token).then((value) {
      labState.loadedItens = value;
      ProductiveActivityService().getAllProductive(authState.token).then((value) {
        for(LaborResponse labor in labState.loadedItens){
          labor.production = value.firstWhere((element) => labor.production!.id == element.id);
        }
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return LaborMenuPage();
      }));
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
    });
  }

  void callCost(
      AppProvider appState, AuthProvider authState, BuildContext context) {
    appState.isOnHome = false;
  }

  void callAnalisys(
      AppProvider appState, AuthProvider authState, BuildContext context) {
    appState.isOnHome = false;
  }

  void fillFields(AppProvider appState, PropertyProvider propState,
      Property value, BuildContext context) {
    appState.accessProfileValue = value.accessProfile!;
    propState.professionController.text = value.profession ?? '';
    propState.nameController.text = value.fullName!;
    propState.cpfController.text = maskCpf(value.cpf.toString());
    propState.dateController.text = dateUnmask(value.birthDate!);
    propState.dddController.text = value.phoneCode.toString();
    propState.phoneController.text = value.phone.toString();
    appState.possessionConditionValue = value.possessionCondition!;
    appState.isOthers = value.possessionConditionDesc == null ? false : true;
    propState.possessionController.text = value.possessionConditionDesc ?? '';
    propState.propertyNameController.text = value.propertyName!;
    propState.cnpjController.text = maskCnpj(value.cnpj.toString());
    appState.unityValue = value.pmu!;
    propState.propertieSizeController.text = value.area.toString();
    appState.ufValue = value.uf!;
    propState.cityController.text = value.city!;
    appState.cityValue = value.city!;
    appState.lat = value.latitude!;
    appState.latController.text = value.latitude!.toString();
    appState.long = value.longitude!;
    appState.longController.text = value.longitude!.toString();
    propState.plusCodeController.text = value.plusCode ?? '';
    propState.emailController.text = value.email!;
    propState.isBiometric = value.biometric!;
  }
}

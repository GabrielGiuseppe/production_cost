import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/presentation/pages/login_page.dart';
import 'package:provider/provider.dart';

import '../../../../shared/constants/app_colors.dart';
import '../../../../shared/presentation/pages/home_layout.dart';
import '../../../../shared/presentation/widgets/my_button.dart';
import '../../../../shared/presentation/widgets/my_information_field.dart';
import '../../../../shared/providers/app_provider.dart';
import '../../../../shared/utils/register_fild_validator.dart';
import '../../data/model/register_new_password_payload.dart';
import '../../data/service/forgot_password_service.dart';

class NewPasswodPage extends StatefulWidget {
  const NewPasswodPage({Key? key}) : super(key: key);

  @override
  State<NewPasswodPage> createState() => _NewPasswodPageState();
}

class _NewPasswodPageState extends State<NewPasswodPage> {
  TextEditingController passwordController = TextEditingController(text: '');
  FocusNode passwordFocusNode = FocusNode();
  TextEditingController confirmPasswordController = TextEditingController(text: '');
  FocusNode confirmPasswordFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return HomeLayout(children: [
       Container(
         padding: EdgeInsets.only(left: 15, right: 15),
         child: Column(children: [
           Text(
             'Cadastrar Nova Senha',
             style: TextStyle(
                 fontSize: 30,
                 fontWeight: FontWeight.w700,
                 color: appState.darkMode
                     ? AppColor.secondaryBlue
                     : AppColor.secondaryBlueLight),
             textAlign: TextAlign.center,
           ),
         SizedBox(height: 30,),
         MyInformationField(
           controller: passwordController,
           fieldTitle: 'Informe a nova Senha',
           focusNode: passwordFocusNode,
           onSubmitted: (value) {
             passwordFocusNode.unfocus();
             FocusScope.of(context).requestFocus(confirmPasswordFocusNode);
           },
           showAlert: appState.invalidFields
               .contains(ValidFields.password)
               ? true
               : false,
           alertMessage: 'Campo obrigatório',
           onTap: () {
             setState(() {
               appState.invalidFields.remove(ValidFields.password);
             });
           },
         ),
         MyInformationField(
           controller: confirmPasswordController,
           fieldTitle: 'Confirme a nova Senha',
           focusNode: confirmPasswordFocusNode,
           onSubmitted: (value) {
             passwordFocusNode.unfocus();
           },
           showAlert: appState.invalidFields
               .contains(ValidFields.confirmPassword)
               ? true
               : false,
           alertMessage: 'Campo obrigatório',
           onTap: () {
             setState(() {
               appState.invalidFields.remove(ValidFields.confirmPassword);
             });
           },
         ),
           MyButton(
             label: 'Enviar Código',
             onPress: () {
               sendPassword(appState);
             },
           ),
       ],),)
      ]);
    });
  }

  void sendPassword(AppProvider appState) {
    appState.invalidFields = [];
    checkField(ValidFields.password, appState, passwordController);
    checkField(ValidFields.confirmPassword, appState, passwordController);

    if(appState.invalidFields.isEmpty){
      RegisterNewPasswordPayload payload = RegisterNewPasswordPayload(
          password: passwordController.text,
          confirmPassword: confirmPasswordController.text);
      ForgotPasswordService().registerNewPassword(payload).then((value) {
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context){
          return LoginPage();
        }), (route) => false);
      }).onError((error, stackTrace) {
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }

    setState(() {

    });
  }
}

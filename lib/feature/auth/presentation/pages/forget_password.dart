import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/service/forgot_password_service.dart';
import 'package:production_cost/feature/auth/presentation/pages/new_password_page.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:provider/provider.dart';

import '../../../../shared/presentation/pages/home_layout.dart';
import '../../../../shared/presentation/widgets/my_information_field.dart';
import '../../../../shared/providers/app_provider.dart';
import '../../../../shared/utils/app_strings.dart';
import '../../../../shared/utils/register_fild_validator.dart';
import '../../data/model/confirm_code_payload.dart';
import '../../data/model/forgot_password_payload.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  TextEditingController emailController = TextEditingController(text: '');
  FocusNode emailFocusNode = FocusNode();
  TextEditingController codeController = TextEditingController(text: '');
  FocusNode codeFocusNode = FocusNode();
  bool emailSent = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return HomeLayout(children: [
        Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            color: appState.darkMode ? AppColor.backgroundDark : Colors.white,
            child: Column(
              children: [
                Text(
                  'Esqueci minha senha',
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w700,
                      color: appState.darkMode
                          ? AppColor.secondaryBlue
                          : AppColor.secondaryBlueLight),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 25,
                ),
                MyInformationField(
                  disabled: emailSent,
                  controller: emailController,
                  fieldTitle: 'Informe o Email Cadastrado',
                  focusNode: emailFocusNode,
                  onSubmitted: (value) {
                    emailFocusNode.unfocus();
                  },
                  showAlert: appState.invalidFields.contains(ValidFields.email)
                      ? true
                      : false,
                  alertMessage: 'Campo obrigatório',
                  onTap: () {
                    setState(() {
                      appState.invalidFields.remove(ValidFields.email);
                    });
                  },
                ),
                emailSent
                    ? MyButton(
                        label: 'Enviar Código',
                        fontColor: appState.darkMode
                            ? AppColor.secondaryBlue
                            : AppColor.secondaryBlueLight,
                        backgroundColor: Colors.transparent,
                        borderColor: appState.darkMode
                            ? AppColor.secondaryBlue
                            : AppColor.secondaryBlueLight,
                      )
                    : MyButton(
                        label: 'Enviar Código',
                        onPress: () {
                          sendEmail(appState);
                        },
                      ),
                SizedBox(
                  height: 25,
                ),
                emailSent
                    ? MyInformationField(
                        controller: codeController,
                        fieldTitle: 'Informe o Email Cadastrado',
                        focusNode: codeFocusNode,
                        onSubmitted: (value) {
                          codeFocusNode.unfocus();
                        },
                        showAlert: appState.invalidFields
                                .contains(ValidFields.password)
                            ? true
                            : false,
                        alertMessage: 'Campo obrigatório',
                        onTap: () {
                          setState(() {
                            appState.invalidFields.remove(ValidFields.password);
                          });
                        },
                      )
                    : SizedBox(),
                emailSent
                    ? MyButton(
                        width: 200,
                        label: 'Confirmar Código',
                  onPress: (){
                          confirmCode(appState);
                  },
                      )
                    : SizedBox(),
              ],
            ))
      ]);
    });
  }

  void sendEmail(AppProvider appState) {
    appState.invalidFields = [];
    checkField(ValidFields.email, appState, emailController);
    if (appState.invalidFields.isEmpty) {
      ForgotPasswordPayload payload = ForgotPasswordPayload(email: emailController.text);
      ForgotPasswordService().forgotPassword(payload).then((value) {
        emailSent = true;
      }).onError((error, stackTrace) {
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });

    }
    setState(() {});
  }

  void confirmCode(AppProvider appState) {
    appState.invalidFields = [];
    checkField(ValidFields.password, appState, codeController);
    if (appState.invalidFields.isEmpty) {
      ConfirmCodePayload payload = ConfirmCodePayload(email: emailController.text, confirmationCode: codeController.text);
      ForgotPasswordService().validateCode(payload).then((value){
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return NewPasswodPage();
        }));
      }).onError((error, stackTrace) {
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }

    setState(() {

    });
  }
}

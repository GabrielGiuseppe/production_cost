import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/model/login_payload.dart';
import 'package:production_cost/feature/auth/data/service/login_dervice.dart';
import 'package:production_cost/feature/auth/presentation/pages/forget_password.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/presentation/widgets/my_information_field.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:production_cost/shared/utils/register_fild_validator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../shared/presentation/pages/home_layout.dart';
import '../../../../shared/presentation/widgets/default_alert.dart';
import '../../../../shared/providers/app_provider.dart';
import '../../../dashboard_page.dart';
import '../../../property/data/service/property_service.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late BuildContext dialogContext;
  TextEditingController emailController = TextEditingController(text: '');
  FocusNode emailNode = FocusNode();
  TextEditingController passwordController = TextEditingController(text: '');
  FocusNode passwordNode = FocusNode();
  bool remember = false;
  bool hidePass = true;

  @override
  void initState() {
    _loadUserEmailPassword();
    super.initState();
  }

  void _handleRemeberme(bool value) {
    remember = value;
    SharedPreferences.getInstance().then(
          (prefs) {
        prefs.setBool("remember_me", value);
        prefs.setString('email', emailController.text);
        prefs.setString('password', passwordController.text);
      },
    );
    setState(() {
      remember = value;
    });
  }

  void _loadUserEmailPassword() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var email = prefs.getString("email") ?? "";
      var password = prefs.getString("password") ?? "";
      var remeberMe = prefs.getBool("remember_me") ?? false;
      if (remeberMe) {
        setState(() {
          remember = true;
        });
        emailController.text = email;
        passwordController.text = password;
      }
    } catch (e) {print(e);}
  }

  @override
  void dispose() {
    if(remember){
      SharedPreferences.getInstance().then(
            (prefs) {
          prefs.setBool("remember_me", remember);
          prefs.setString('email', emailController.text);
          prefs.setString('password', passwordController.text);
        },
      );
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return HomeLayout(children: [
          Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                MyInformationField(
                  controller: emailController,
                  fieldTitle: 'Email',
                  focusNode: emailNode,
                    onSubmitted: (value) {
                      emailNode.unfocus();
                      FocusScope.of(context).requestFocus(passwordNode);
                    },
                showAlert: appState.invalidFields.contains(ValidFields.email)
                ? true
                : false,
            alertMessage:
            'Campo obrigatório',
            onTap: (){
              setState(() {
                appState.invalidFields.remove(ValidFields.email);
              });
            },
                ),
                MyInformationField(
                  hasIcon: true,
                  iconTap: (){
                    hidePass = !hidePass;
                    setState(() {
                    });
                  },
                  obscureText: hidePass,
                  controller: passwordController,
                  fieldTitle: 'Senha',
                focusNode: passwordNode,
                  showAlert: appState.invalidFields.contains(ValidFields.password)
                      ? true
                      : false,
                  alertMessage:
                  'Campo obrigatório',
                  onTap: (){
                    setState(() {
                      appState.invalidFields.remove(ValidFields.password);
                    });
                  },
                ),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      child: Row(
                        children: [
                          Checkbox(
                              value: remember,
                              onChanged: (value) {
                                setState(() {
                                  _handleRemeberme(value!);
                                });
                              }),
                           Text(
                            'Lembrar',
                            style: TextStyle(
                                fontSize: 18, color: appState.darkMode?Colors.white:AppColor.font),
                          ),
                        ],
                      ),
                    )),
                    Expanded(
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                              return ForgotPasswordPage();
                            }));
                          },
                      child: Text(AppStrings.forgetPassword,
                        style: TextStyle(
                            fontSize: 18, color: appState.darkMode? AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
                    ))
                  ],
                ),
               const SizedBox(height: 25,),
               MyButton(
                  label: AppStrings.logInLongLabel,
                  width: 250,
                 onPress: (){
                    validateFields(appState,authState,context);
                 },
                )
              ],
            ),
          )
        ]);
      });
    });
  }

  void validateFields(AppProvider appState,AuthProvider authState, BuildContext context) {
    appState.invalidFields = [];
    if(emailController.text == ''){
      appState.invalidFields.add(ValidFields.email);
    }
    if(passwordController.text == ''){
      appState.invalidFields.add(ValidFields.password);
    }
    setState(() {
    });
    if(appState.invalidFields.isEmpty){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            dialogContext = context;
            return const DefaultAlert(
              text: "Autenticando...",
            );
          },
          useSafeArea: true,
          barrierDismissible: false);
      LoginPayload payload = LoginPayload(email: emailController.text, password: passwordController.text);
      LoginService().logIn(appState, payload).then((value) {
        authState.token = value;
        authState.isLoggedIn = true;
        PropertyService().getProperty(authState).then((value) {
          authState.usePmu = value.pmu!;
        Navigator.pop(dialogContext);
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return DashboradPage();
        }));
      }).onError((error, stackTrace) {
        Navigator.pop(dialogContext);
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });}).onError((error, stackTrace){
        Navigator.pop(dialogContext);
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }else{
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}

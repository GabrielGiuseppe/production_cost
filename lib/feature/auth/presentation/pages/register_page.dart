import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/model/property.dart';
import 'package:production_cost/feature/informarion/presentation/accept_terms_page.dart';
import 'package:production_cost/feature/main_page.dart';
import 'package:production_cost/shared/presentation/widgets/my_auto_complete_city_field.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/utils/input_masks.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../../../../shared/constants/app_colors.dart';
import '../../../../shared/presentation/pages/form_layout.dart';
import '../../../../shared/presentation/widgets/checkbox_text.dart';
import '../../../../shared/presentation/widgets/default_alert.dart';
import '../../../../shared/presentation/widgets/my_date_picker_field.dart';
import '../../../../shared/presentation/widgets/my_dropdown_field.dart';
import '../../../../shared/presentation/widgets/my_information_field.dart';
import '../../../../shared/presentation/widgets/text_switch.dart';
import '../../../../shared/utils/string_utils.dart';
import '../../../../shared/utils/register_fild_validator.dart';
import '../../data/service/register_service.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);


  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late BuildContext dialogContext;
  TextEditingController professionController = TextEditingController(text: '');
  FocusNode professionNode = FocusNode();
  TextEditingController nameController = TextEditingController(text: '');
  FocusNode nameNode = FocusNode();
  TextEditingController cpfController = TextEditingController(text: '');
  FocusNode cpfNode = FocusNode();
  TextEditingController dateController = TextEditingController(text: '');
  FocusNode dateNode = FocusNode();
  TextEditingController dddController = TextEditingController(text: '');
  FocusNode dddNode = FocusNode();
  TextEditingController phoneController = TextEditingController(text: '');
  FocusNode phoneNode = FocusNode();
  TextEditingController possessionController = TextEditingController(text: '');
  FocusNode possessionNode = FocusNode();
  TextEditingController propertyNameController =
  TextEditingController(text: '');
  FocusNode propertyNameNode = FocusNode();
  TextEditingController cnpjController = TextEditingController(text: '');
  FocusNode cnpjNode = FocusNode();
  TextEditingController propertieSizeController =
  TextEditingController(text: '');
  FocusNode propertieSizeNode = FocusNode();
  FocusNode cityNode = FocusNode();
  FocusNode latNode = FocusNode();
  FocusNode longNode = FocusNode();
  TextEditingController plusCodeController = TextEditingController(text: '');
  FocusNode plusCodeNode = FocusNode();
  TextEditingController emailController = TextEditingController(text: '');
  FocusNode emailNode = FocusNode();
  TextEditingController passwordController = TextEditingController(text: '');
  FocusNode passwordNode = FocusNode();
  TextEditingController repeatPasswordController =
  TextEditingController(text: '');
  FocusNode repeatPasswordNode = FocusNode();

  bool isBiometric = false;
  bool isAcceptTnC = false;
  bool hidePass = true;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return FormLayout(
        pageTitle: 'Cadastrar',
        children: [
          MyDropDownField(
            initialValue: appState.accessProfileValue,
            valuesList: appState.accessProfileList,
            fieldTitle: 'Perfil de Acesso *',
            fieldType: FieldType.profission,
          ),
          appState.isProfissional
              ? MyInformationField(
            showAlert: appState.invalidFields.contains(ValidFields.profession)
                ? true
                : false,
            alertMessage:
            'Ao escolher profissional este campo deve ser preenchido',
            controller: professionController,
            fieldTitle: 'Profissão',
            focusNode: professionNode,
            onSubmitted: (value) {
              professionNode.unfocus();
              FocusScope.of(context).requestFocus(nameNode);
            },
            onTap: () {
              setState(() {
                appState.invalidFields.remove(ValidFields.profession);
              });
            },
          )
              : const SizedBox(),
          MyInformationField(
              controller: nameController,
              fieldTitle: 'Nome Completo *',
              focusNode: nameNode,
              showAlert: appState.invalidFields.contains(ValidFields.fullName)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.fullName);
                });
              },
              onSubmitted: (value) {
                nameNode.unfocus();
                FocusScope.of(context).requestFocus(cpfNode);
              }),
          MyInformationField(
              controller: cpfController,
              fieldTitle: 'CPF *',
              inputFormatters: [InputMasks.cpfMask()],
              textInputType: TextInputType.number,
              focusNode: cpfNode,
              showAlert: appState.invalidFields.contains(ValidFields.cpf)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.cpf);
                });
              },
              onSubmitted: (value) {
                cpfNode.unfocus();
                FocusScope.of(context).requestFocus(dateNode);
              }),
          MyDatePickerField(
            controller: dateController,
            focusNode: dateNode,
            nextNode: dddNode,
            keyboardType: TextInputType.number,
            fieldTitle: 'Data de Nascimento *',
            inputFormatters: [InputMasks.dateMask()],
              showAlert: appState.invalidFields.contains(ValidFields.birthDate)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.birthDate);
                });
              },
            onPress: () async {
              DateTime initialTime = DateTime.now();
              if (dateController.text.isNotEmpty) {
                initialTime = StringUtils().convertStringToDate(dateController.text, "dd/MM/yyyy");
              }

              DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: initialTime,
                  firstDate: DateTime(1800),
                  lastDate: DateTime.now());

              if (pickedDate != null) {
                String formattedDate = DateFormat("dd/MM/yyyy").format(pickedDate);
                setState(() {
                  dateController.text = formattedDate;
                });
              }
            },
              onSubmitted: (value) {
                dateNode.unfocus();
                FocusScope.of(context).requestFocus(dddNode);
              }
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: MyInformationField(
                  controller: dddController,
                  width: 70,
                  fieldTitle: 'DDD',
                  inputFormatters: [InputMasks.dddMask()],
                  textInputType: TextInputType.number,
                  focusNode: dddNode,
                  showAlert: appState.invalidFields.contains(ValidFields.phoneCode) || appState.invalidFields.contains(ValidFields.phone)
                      ? true
                      : false,
                  alertMessage:
                  '*',
                  onSubmitted: (value) {
                    dddNode.unfocus();
                    FocusScope.of(context).requestFocus(phoneNode);
                  },
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  flex: 4,
                  child: MyInformationField(
                    controller: phoneController,
                    fieldTitle: 'Telefone *',
                    inputFormatters: [InputMasks.phoneMask()],
                    textInputType: TextInputType.number,
                    focusNode: phoneNode,
                    showAlert: appState.invalidFields.contains(ValidFields.phoneCode) || appState.invalidFields.contains(ValidFields.phone)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: (){
                      setState(() {
                        appState.invalidFields.remove(ValidFields.phone);
                        appState.invalidFields.remove(ValidFields.phoneCode);
                      });
                    },
                  ))
            ],
          ),
          MyDropDownField(
            initialValue: appState.possessionConditionValue,
            valuesList: appState.possessionConditionlist,
            fieldTitle: 'Condição da Posse *',
            fieldType: FieldType.possession,
          ),
          appState.isOthers
              ? MyInformationField(
              controller: possessionController,
              fieldTitle: 'Descreva a Condição de Posse *',
              focusNode: possessionNode,
              showAlert: appState.invalidFields.contains(ValidFields.possessionConditionDesc)
                  ? true
                  : false,
              alertMessage:
              'Ao escolher Outros este campo deve ser preenchido',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.possessionConditionDesc);
                });
              },
              onSubmitted: (value) {
                possessionNode.unfocus();
                FocusScope.of(context).requestFocus(propertyNameNode);
              })
              : const SizedBox(),
          MyInformationField(
              controller: propertyNameController,
              fieldTitle: 'Nome da Propriedade *',
              focusNode: propertyNameNode,
              showAlert: appState.invalidFields.contains(ValidFields.propertyName)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.propertyName);
                });
              },
              onSubmitted: (value) {
                propertyNameNode.unfocus();
                FocusScope.of(context).requestFocus(cnpjNode);
              }),
          MyInformationField(
            controller: cnpjController,
            fieldTitle: 'CNPJ da Propriedade *',
            showAlert: appState.invalidFields.contains(ValidFields.cnpj)
                ? true
                : false,
            alertMessage:
            'Campo obrigatório',
            onTap: (){
              setState(() {
                appState.invalidFields.remove(ValidFields.cnpj);
              });
            },
            inputFormatters: [InputMasks.cnpjMask()],
            textInputType: TextInputType.number,
            focusNode: cnpjNode,
          ),
          MyDropDownField(
            valuesList: appState.unityList,
            initialValue: appState.unityValue,
            fieldTitle: 'Unidade de Medida da Propriedade *',
          ),
          MyInformationField(
            controller: propertieSizeController,
            fieldTitle: 'Tamanho da área da propriedade *',
            showAlert: appState.invalidFields.contains(ValidFields.area)
                ? true
                : false,
            alertMessage:
            'Campo obrigatório',
            onTap: (){
              setState(() {
                appState.invalidFields.remove(ValidFields.area);
              });
            },
            focusNode: propertieSizeNode,
            textInputType: TextInputType.number,
          ),
          MyDropDownField(
            valuesList: appState.ufList,
            initialValue: appState.ufValue,
            fieldType: FieldType.uf,
            fieldTitle: 'Selecione a UF *',
          ),
           MyAutoCompleteCityField(
             prefillText: appState.cityValue,
            fieldTitle: 'Digite e Selecione o Município *',
              onSubmitted: (value) {
                FocusScope.of(context).requestFocus(plusCodeNode);
              }
          ),
          Text('Localização da propriedade *',
            style: TextStyle(fontSize: 18, color:appState.darkMode? AppColor.secondaryBlue:AppColor.secondaryBlueLight, fontWeight: FontWeight.w700),),
          const SizedBox(height: 25,),
          MyInformationField(
            disabled: true,
              controller: appState.latController,
              fieldTitle: 'Latitude',
              focusNode: latNode,
              onSubmitted: (value) {
                latNode.unfocus();
                FocusScope.of(context).requestFocus(longNode);
              }),
          MyInformationField(
            disabled: true,
              controller: appState.longController,
              fieldTitle: 'Logitude',
              focusNode: longNode,
              onSubmitted: (value) {
                longNode.unfocus();
                FocusScope.of(context).requestFocus(plusCodeNode);
              }),
          MyInformationField(
              controller: plusCodeController,
              fieldTitle: 'Informe o Plus Code da propriedade',
              focusNode: plusCodeNode,
              onSubmitted: (value) {
                plusCodeNode.unfocus();
                FocusScope.of(context).requestFocus(emailNode);
              }),
          MyInformationField(
            textCapitalization: TextCapitalization.none,
              controller: emailController,
              fieldTitle: 'Informe o seu e-mail *',
              showAlert: appState.invalidFields.contains(ValidFields.email)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.email);
                });
              },
              focusNode: emailNode,
              onSubmitted: (value) {
                emailNode.unfocus();
                FocusScope.of(context).requestFocus(passwordNode);
              }),
          MyInformationField(
              hasIcon: true,
              iconTap: (){
                hidePass = !hidePass;
                setState(() {
                });
              },
              obscureText: hidePass,
              textCapitalization: TextCapitalization.none,
              controller: passwordController,
              fieldTitle: 'Cadastre uma senha *',
              showAlert: appState.invalidFields.contains(ValidFields.password)
                  ? true
                  : false,
              alertMessage:
              'Campo obrigatório',
              onTap: (){
                setState(() {
                  appState.invalidFields.remove(ValidFields.password);
                });
              },
              focusNode: passwordNode,
              onSubmitted: (value) {
                passwordNode.unfocus();
                FocusScope.of(context).requestFocus(repeatPasswordNode);
              }),
          MyInformationField(
            hasIcon: true,
            iconTap: (){
              hidePass = !hidePass;
              setState(() {
              });
            },
            obscureText: hidePass,
            textCapitalization: TextCapitalization.none,
            controller: repeatPasswordController,
            fieldTitle: 'Confirme a senha *',
            showAlert: appState.invalidFields.contains(ValidFields.confirmPassword)
                ? true
                : false,
            alertMessage:
            'Campo obrigatório',
            onTap: (){
              setState(() {
                appState.invalidFields.remove(ValidFields.confirmPassword);
              });
            },
            focusNode: repeatPasswordNode,
          ),
          TextSwitch(
              onChanged: (bool value) {
                setState(() {
                  isBiometric = value;
                });
              },
              label: "Solicitar Biometria ou Face ID",
              initialValue: isBiometric),
          const SizedBox(
            height: 25,
          ),
          InkWell(
            onTap: (() {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const TermsPage();
              }));
            }),
            child: Text(
              "Leia os termos de aceite do aplicativo",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: appState.darkMode
                    ? AppColor.secondaryBlue
                    : AppColor.secondaryBlueLight,
                fontSize: 18,
              ),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          CheckboxText(
              onChange: (bool? value) {
                setState(() {
                  isAcceptTnC = value!;
                });
              },
              label: "Concordo com os termos de aceite",
              initialValue: isAcceptTnC),
          const SizedBox(
            height: 25,
          ),
          Row(
            children: [
              Expanded(
                  child: MyButton(
                    label: 'Efetuar Cadastro',
                    fontSize: 25,
                    onPress: () {
                      validateFields(appState, context);
                    },
                  ))
            ],
          )
        ],
      );
    });
  }

  void validateFields(AppProvider appState, BuildContext context) {
    Property payload = Property();
    appState.invalidFields = [];
         appState.accessProfileValue;
         checkField(ValidFields.profession, appState, professionController);
         checkField(ValidFields.fullName, appState, nameController);
         checkField(ValidFields.cpf, appState,cpfController);
         checkField(ValidFields.birthDate, appState, dateController);
         checkField(ValidFields.phoneCode, appState,dddController);
         checkField(ValidFields.phone, appState, phoneController);
         appState.possessionConditionValue;
         checkField(ValidFields.possessionConditionDesc, appState, possessionController);
         checkField(ValidFields.propertyName, appState, propertyNameController);
         checkField(ValidFields.cnpj, appState, cnpjController);
         checkField(ValidFields.area, appState, propertieSizeController);
         checkField(ValidFields.city, appState,TextEditingController());
         checkField(ValidFields.email, appState, emailController);
         checkField(ValidFields.password, appState, passwordController);
         checkField(ValidFields.confirmPassword, appState, repeatPasswordController);

    if (appState.invalidFields.isNotEmpty) {
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }else{
      payload = Property(
          accessProfile: appState.accessProfileValue,
          profession: checkField(ValidFields.profession, appState, professionController),
          fullName: checkField(ValidFields.fullName, appState, nameController),
          cpf: int.parse(checkField(ValidFields.cpf, appState,cpfController)),
          birthDate: dateMask(checkField(ValidFields.birthDate, appState, dateController)),
          phoneCode: int.parse(checkField(ValidFields.phoneCode, appState,dddController)),
          phone: int.parse(checkField(ValidFields.phone, appState, phoneController)),
          possessionCondition: appState.possessionConditionValue,
          possessionConditionDesc: checkField(ValidFields.possessionConditionDesc, appState, possessionController),
          propertyName: checkField(ValidFields.propertyName, appState, propertyNameController),
          cnpj: int.parse(checkField(ValidFields.cnpj, appState, cnpjController)),
          pmu: appState.unityValue,
          area: int.parse(checkField(ValidFields.area, appState, propertieSizeController)),
          uf: appState.ufValue,
          city: checkField(ValidFields.city, appState,TextEditingController()),
          latitude: double.parse(checkField(ValidFields.latitude, appState, appState.latController)),
          longitude: double.parse(checkField(ValidFields.longitude, appState, appState.longController)),
          plusCode: plusCodeController.text == ""? null:plusCodeController.text,
          email: checkField(ValidFields.email, appState, emailController),
          password: checkField(ValidFields.password, appState, passwordController),
          confirmPassword: checkField(ValidFields.confirmPassword, appState, repeatPasswordController),
          biometric: isBiometric);
    }
    setState(() {});
    if(isAcceptTnC){
     registerUser(appState, payload, context);
    }else{
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Aceite os termos para continuar",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void registerUser(AppProvider appState, Property payload, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Efetuando o cadastro...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    RegisterService().registerProperty(appState, payload).then((value) {
      Navigator.pop(dialogContext);
      openregisterDialog(appState, context);
    }).onError((error, stackTrace) {
      Navigator.pop(context);
      print(error);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void openregisterDialog(AppProvider appState, BuildContext context) {
    Widget okButton = Expanded(child: MyButton(
      label: 'Retornar ao menu',
      onPress: (){
        Navigator.of(context).pop();
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context){
          return const MainPage();
        }), (route) => false);
      },
    ));
    AlertDialog alert = AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text(
        'Cadastro efetuado com sucesso!',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
      ),
      content: Text(
        textAlign: TextAlign.center,
        'Faça o login com seu email e senha',
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
      ),
      actions: [okButton],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
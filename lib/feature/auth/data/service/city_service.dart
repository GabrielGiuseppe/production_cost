import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:production_cost/feature/auth/data/model/city_model.dart';

class CityService {
  Future<CityModelList> getCities(String uf) async {
    var response = await call(uf);
    if (response.statusCode == 200) {
      return CityModelList.toJson(jsonDecode(response.body));
    }else{
      throw Exception('Não foi possivel carregar as Cidades');
    }
  }

  call(String uf) {
    return http.get(
        Uri.parse(
            'https://servicodados.ibge.gov.br/api/v1/localidades/estados/${uf.trim()}/municipios'),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        });
  }
}

import 'dart:async';
import 'dart:convert';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:http/http.dart' as http;

import '../model/confirm_code_payload.dart';
import '../model/forgot_password_payload.dart';
import '../model/register_new_password_payload.dart';

class ForgotPasswordService {
  String host = AppStrings.host;

  Future<dynamic> forgotPassword(ForgotPasswordPayload payload) async {
    final response = await http.post(
        Uri.parse(
            '$host/auth/request-reset-password'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(payload));

    if (response.statusCode == 201) {
      return response.body;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> registerNewPassword(RegisterNewPasswordPayload payload) async {
    final response  = await http.post(
        Uri.parse(
            '$host/auth/reset-password'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(payload)
    );

    if (response.statusCode == 201) {
      return response.body;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> validateCode(ConfirmCodePayload payload) async {
    final response  = await http.post(
        Uri.parse(
            '$host/auth/validate-confirmation-code'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(payload)
    );

    if (response.statusCode == 201) {
      return response.body;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }
}

import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:production_cost/feature/auth/data/model/login_payload.dart';
import 'package:production_cost/shared/providers/app_provider.dart';

import '../../../../shared/utils/app_strings.dart';

class LoginService{
  Future<String> logIn(AppProvider appState, LoginPayload payload) async{
    var response = await call(appState, payload);
    if(response.statusCode == 201){
      return jsonDecode(response.body)['access_token'];
    }else{
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  call(AppProvider appState, LoginPayload payload) {
    return http.post(Uri.parse('${AppStrings.host}/auth/login'),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: jsonEncode(payload.toJson()));
  }
}
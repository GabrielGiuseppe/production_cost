import 'dart:convert';
import 'dart:io';

import 'package:production_cost/feature/auth/data/model/property.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:http/http.dart' as http;
import 'package:production_cost/shared/utils/app_strings.dart';
class RegisterService{
  Future<void> registerProperty(AppProvider appState, Property payload) async{
    Map<String, dynamic> sending = jsonDecode(jsonEncode(payload));
    print(jsonEncode(payload));
    var response = await call(appState, payload);
    if(response.statusCode == 201){

    }else{
      throw Exception(jsonDecode(response.body)['message']);
    }

  }

  call(AppProvider appState, Property payload) {
    return http.post(Uri.parse('${AppStrings.host}/auth/register'),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
        body: jsonEncode(payload.toJson()));
  }
}
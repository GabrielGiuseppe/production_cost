class CityModelList{
  List<String>? cityModelList;

  CityModelList({this.cityModelList});

  factory CityModelList.toJson(List<dynamic> json){
    var cities = json;
    List<String> citiesList = [];
    for(var city in cities){
      citiesList.add(city['nome']);
    }
    return CityModelList(cityModelList: citiesList);
  }

}
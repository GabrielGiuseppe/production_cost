class RegisterNewPasswordPayload {
  String password;
  String confirmPassword;

  RegisterNewPasswordPayload({
    required this.password,
    required this.confirmPassword
  });

  Map<String, dynamic> toJson() {
    return {
      "password": password,
      "confirmPassword": confirmPassword
    };
  }
}

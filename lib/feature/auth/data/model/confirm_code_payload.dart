class ConfirmCodePayload {
  String email;
  String confirmationCode;

  ConfirmCodePayload({
    required this.email,
    required this.confirmationCode
  });

  Map<String, dynamic> toJson() {
    return {
      "email": email,
      "confirmationCode": confirmationCode
    };
  }
}

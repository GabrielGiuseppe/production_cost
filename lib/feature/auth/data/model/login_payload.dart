class LoginPayload{
  String? email;
  String? password;

  LoginPayload({this.email, this.password});

  Map<String, dynamic> toJson(){
    return {
      'email':email,
      'password':password
    };
  }
}
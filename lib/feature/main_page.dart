import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/pages/home_layout.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:provider/provider.dart';

import '../shared/presentation/widgets/default_alert.dart';
import '../shared/presentation/widgets/my_button.dart';
import 'auth/data/service/city_service.dart';
import 'auth/presentation/pages/login_page.dart';
import 'auth/presentation/pages/register_page.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      appState.setDeviceWidth(MediaQuery.of(context).size.width);
      appState.setDeviceHeight(MediaQuery.of(context).size.height);
      return HomeLayout(children: [
        Container(
          padding: const EdgeInsets.only(left: 15, right: 15),
          color: appState.darkMode ? AppColor.backgroundDark : Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStrings.appTitle,
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                    color: appState.darkMode
                        ? AppColor.secondaryBlue
                        : AppColor.secondaryBlueLight),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                AppStrings.objectiveTitle,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: appState.darkMode ? Colors.white : AppColor.font),
              ),
              const SizedBox(height: 10,),
              Text(AppStrings.objectiveText,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: appState.darkMode ? Colors.white : AppColor.font)),
              const Divider(thickness: 2,),
              const SizedBox(
                height: 15,
              ),
              Text(
                AppStrings.participateTitle,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: appState.darkMode ? Colors.white : AppColor.font),
              ),
              const SizedBox(height: 10,),
              Text(AppStrings.participateText,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: appState.darkMode ? Colors.white : AppColor.font)),
              const Divider(thickness: 2,),
              const SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                MyButton(
                  onPress: () => registerPage(context, appState),
                  label: AppStrings.registerButtonLabel,),
                MyButton(
                  onPress: () => loginPage(context, appState),
                  label: AppStrings.logInShortLabel,),
              ],
              ),
              const SizedBox(height: 30,),
            ],
          ),
        )
      ]);
    });
  }

  registerPage(BuildContext context, AppProvider appState) {
    late BuildContext dialogContext;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Obtendo localização...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    appState.isOnHome = false;
    appState.setAccessProfileValue(appState.accessProfileList.first);
    appState.setPossessionConditionValue(appState.possessionConditionlist.first);
    appState.setUnityValue(appState.unityList.first);
    appState.setUfValue(appState.ufList.elementAt(24));
    appState.getLocation().then((value) {
      appState.lat = value.latitude;
      appState.long = value.longitude;
      appState.latController.text = appState.lat.toString();
      appState.longController.text = appState.long.toString();
    CityService().getCities(appState.ufValue).then((value) {
      appState.cityList = value;
      Navigator.pop(dialogContext);
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return const RegisterPage();
    }));
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
    });
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Favor habilitar a localização para prosseguir",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  loginPage(BuildContext context, AppProvider appState) {
    appState.isOnHome = false;
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return const LoginPage();
    }));
  }
}

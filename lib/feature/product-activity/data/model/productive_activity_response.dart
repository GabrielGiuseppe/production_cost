class ProductiveActivityResponse{
  ProductiveActivityNameResponse? activityName;
  String? technology;
  String? pmu;
  int? size;
  int? expectation;
  String? measurementUnit;
  int? capacity;
  double? insuranceCost;
  double? technicalAssistanceCost;
  double? yearlyInterestCost;
  int? monthsUnderInterest;
  double? taxCESSR;
  double? yearlyOperationCostOfTraining;
  int? productiveLifeCost;
  String? id;

  ProductiveActivityResponse({
    this.activityName,
    this.technology,
    this.pmu,
    this.size,
    this.expectation,
    this.measurementUnit,
    this.capacity,
    this.insuranceCost,
    this.technicalAssistanceCost,
    this.yearlyInterestCost,
    this.monthsUnderInterest,
    this.taxCESSR,
    this.yearlyOperationCostOfTraining,
    this.productiveLifeCost,
    this.id
  });

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "productiveActivity": activityName,
      "technology": technology,
      "pmu": pmu,
      "size": size,
      "expectation": expectation,
      "measurementUnit": measurementUnit,
      "capacity": capacity,
      "insuranceCost": insuranceCost,
      "monthsUnderInterest": monthsUnderInterest,
      "taxOfCESR":taxCESSR,
      "technicalAssistanceCost": technicalAssistanceCost,
      "yearlyInterestCost": yearlyInterestCost,
      "yearlyOperationCostOfTraining": yearlyOperationCostOfTraining,
      "productiveLifeCost": productiveLifeCost,
    };
  }

  factory ProductiveActivityResponse.fromJson(Map<String, dynamic> json) {
    return ProductiveActivityResponse(
      id: json['id'],
      activityName: json['cultureAndVariant'] == null? ProductiveActivityNameResponse():ProductiveActivityNameResponse.fromJson( json['cultureAndVariant']),
      technology: json['technology'],
      pmu: json['pmu'],
      size: json['size'],
      expectation: json['expectation'],
      measurementUnit: json['measurementUnit'],
      capacity: json['capacity'],
      insuranceCost: double.parse(json['insuranceCost'].toString()),
      technicalAssistanceCost: double.parse(json['technicalAssistanceCost'].toString()),
      yearlyInterestCost: double.parse(json['yearlyInterestCost'].toString()),
      monthsUnderInterest: json['monthsUnderInterest'],
      taxCESSR: double.parse(json['taxOfCESR'].toString()),
      yearlyOperationCostOfTraining: double.parse(json['yearlyOperationCostOfTraining'].toString()),
      productiveLifeCost: json['productiveLifeCost'],
    );
  }
}
class ProductiveActivityNameResponse {
  String? id;
  String? cultureId;
  String? variantId;
  String? description;

  ProductiveActivityNameResponse({
    this.id,
    this.cultureId,
    this.variantId,
    this.description
  });

  factory ProductiveActivityNameResponse.fromJson(Map<String, dynamic> json) {
    return ProductiveActivityNameResponse(
        id: json['id'],
        cultureId: json['cultureId'],
        variantId: json['variantId'],
        description: json['description']
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "cultureId" : cultureId,
      "variantId": variantId,
      "description": description,
    };
  }

}



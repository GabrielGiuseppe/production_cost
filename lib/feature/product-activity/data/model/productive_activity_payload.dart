import 'package:production_cost/feature/product-activity/data/model/productive_activity_response.dart';

class ProductiveActivityPayload {
  ProductiveActivityNameResponse? activityName;
  String? technology;
  String? pmu;
  int? size;
  int? expectation;
  String? measurementUnit;
  int? capacity;
  double? insuranceCost;
  int? monthsUnderInterest;
  double? taxCESSR;
  double? technicalAssistanceCost;
  double? yearlyInterestCost;
  double? yearlyOperationCostOfTraining;
  int? productiveLifeCost;

  ProductiveActivityPayload({
    this.activityName,
    this.technology,
    this.pmu,
    this.size,
    this.expectation,
    this.measurementUnit,
    this.capacity,
    this.insuranceCost,
    this.monthsUnderInterest,
    this.taxCESSR,
    this.technicalAssistanceCost,
    this.yearlyInterestCost,
    this.yearlyOperationCostOfTraining,
    this.productiveLifeCost,
  });

  Map<String, dynamic> toJson() {
    return {
      "cultureAndVariant": activityName,
      "technology": technology,
      "pmu": pmu,
      "size": size,
      "expectation": expectation,
      "measurementUnit": measurementUnit,
      "capacity": capacity,
      "insuranceCost": insuranceCost,
      "monthsUnderInterest": monthsUnderInterest,
      "taxOfCESR":taxCESSR,
      "technicalAssistanceCost": technicalAssistanceCost,
      "yearlyInterestCost": yearlyInterestCost,
      "yearlyOperationCostOfTraining": yearlyOperationCostOfTraining,
      "productiveLifeCost": productiveLifeCost,
    };
  }
}

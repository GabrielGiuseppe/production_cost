import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:production_cost/shared/utils/app_strings.dart';

import '../model/productive_activity_payload.dart';
import '../model/productive_activity_response.dart';

class ProductiveActivityService {
  String host = AppStrings.host;

  Future<ProductiveActivityResponse> addProductive(ProductiveActivityPayload payload, String token) async {
    final response = await http.post(
        Uri.parse(
            '$host/productive-activities'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));
    if (response.statusCode == 201) {
      return ProductiveActivityResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<ProductiveActivityResponse> editProductive(ProductiveActivityResponse payload, String token) async {
    final response = await http.put(
        Uri.parse(
            '$host/productive-activities'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));

    if (response.statusCode == 200) {
      return ProductiveActivityResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> deleteProductive(String id, String token) async {
    final response = await http.delete(
        Uri.parse(
            '$host/productive-activities/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      return "success";
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<ProductiveActivityResponse> getProductive(String id, String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/productive-activities/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      return ProductiveActivityResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<List<ProductiveActivityResponse>> getAllProductive(String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/productive-activities'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });
    String responseJson = jsonDecode(response.body).toString();
    if (response.statusCode == 200) {
      List<ProductiveActivityResponse> listProductive = (json.decode(response.body) as List)
          .map((data) => ProductiveActivityResponse.fromJson(data))
          .toList();
      return listProductive;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<List<ProductiveActivityNameResponse>> searchProductiveName(String token, String keyword) async {
    final response = await http.get(
        Uri.parse(
            '$host/masters/culture-and-variant/search?keyword=$keyword'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson.map((m) => ProductiveActivityNameResponse.fromJson(m)).toList();
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }
}

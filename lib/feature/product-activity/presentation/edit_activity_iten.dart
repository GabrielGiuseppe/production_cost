import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/product-activity/data/model/productive_activity_response.dart';
import 'package:production_cost/feature/product-activity/data/service/productive_activity_service.dart';
import 'package:production_cost/feature/product-activity/presentation/activity_menu_page.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_auto_complete_city_field.dart';
import 'package:production_cost/shared/presentation/widgets/my_dropdown_field.dart';
import 'package:provider/provider.dart';

import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_autocomplete_activity_field.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/presentation/widgets/my_information_field.dart';
import '../../../shared/providers/activity_provider.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import '../../../shared/utils/register_fild_validator.dart';
import '../data/model/productive_activity_payload.dart';

class EditActivityItenPage extends StatefulWidget {
  const EditActivityItenPage({Key? key}) : super(key: key);

  @override
  State<EditActivityItenPage> createState() => _EditActivityItenPage();
}

class _EditActivityItenPage extends State<EditActivityItenPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<ActivityProvider>(builder: (context, actState, child) {
          return FormLayout(pageTitle: 'Editar', children: [
            MyAutoCompleteActivityField(
              focusNode: actState.activityNameNode,
              fieldTitle: 'Atividade Produtiva',
              prefillText: actState.activityNameController.text,
              onSubmitted: (value) {
                actState.activityNameNode.unfocus();
              },
            ),
            MyDropDownField(
              fieldTitle: 'Tecnologia',
              valuesList: actState.tecnologies,
              initialValue: actState.chosenTecnology,
              fieldType: FieldType.tecnologies,
            ),
            Text(
              'Unidade de Medida da Propriedade',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            Text(
              authState.usePmu,
              style: const TextStyle(
                  color: AppColor.redColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 25,
            ),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Tamanho da Atividade Produtiva',
                showAlert: appState.invalidFields.contains(ValidFields.size)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.size);
                  });
                },
                focusNode: actState.sizeNode,
                onSubmitted: (value) {
                  actState.sizeNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.expectationNode);
                },

                controller: actState.sizeController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Expectativa de Produção',
                focusNode: actState.expectationNode,
                onSubmitted: (value) {
                  actState.expectationNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.insuranceCostNode);
                },
                showAlert: appState.invalidFields.contains(ValidFields.expectation)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.expectation);
                  });
                },
                controller: actState.expectationController),
            MyDropDownField(
              fieldTitle: 'Unidade de Medida da Produção',
              fieldType: FieldType.mesureUnity,
              valuesList: actState.unityList,
              initialValue: actState.chosenUnity,
            ),
            actState.isSubjectiveUnity
                ? MyInformationField(
                fieldTitle: 'Capacidade da Medida Escolhida',
                textInputType: TextInputType.number,
                showAlert: appState.invalidFields.contains(ValidFields.measurementUnit)
                    ? true
                    : false,
                alertMessage:
                'Ao escolher Caixa ou Saca este campo deve ser preenchido',
                focusNode: actState.capacityCodeNode,
                onSubmitted: (value) {
                  actState.capacityCodeNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.insuranceCostNode);
                },
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.measurementUnit);
                  });
                },
                controller: actState.capacityCodeController)
                : const SizedBox(),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Valor do Seguro - Custo R\$',
                inputFormatters: [CurrencyTextInputFormatter(
                    decimalDigits: 2,
                    symbol: 'R\$'
                )],
                showAlert: appState.invalidFields.contains(ValidFields.insuranceCost)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.insuranceCost);
                  });
                },
                focusNode: actState.insuranceCostNode,
                onSubmitted: (value) {
                  actState.insuranceCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.technicalAssistanceCostNode);
                },
                controller: actState.insuranceCostController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Valor da Assistência Técnica - Custo R\$',
                showAlert: appState.invalidFields.contains(ValidFields.technicalAssistanceCost)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                inputFormatters: [CurrencyTextInputFormatter(
                    decimalDigits: 2,
                    symbol: 'R\$'
                )],
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.technicalAssistanceCost);
                  });
                },
                focusNode: actState.technicalAssistanceCostNode,
                onSubmitted: (value) {
                  actState.technicalAssistanceCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.yearlyInterestCostNode);
                },
                controller: actState.technicalAssistanceCostController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Taxa de Juros %',
                showAlert: appState.invalidFields.contains(ValidFields.yearlyInterestCost)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.yearlyInterestCost);
                  });
                },
                focusNode: actState.yearlyInterestCostNode,
                onSubmitted: (value) {
                  actState.yearlyInterestCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.yearlyOperationCostOfTrainingNode);
                },
                controller: actState.yearlyInterestCostController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Meses Sobre Juros',
                showAlert: appState.invalidFields.contains(ValidFields.monthsUnderInterest)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.monthsUnderInterest);
                  });
                },
                focusNode: actState.monthUnderInterestInterestCostNode,
                onSubmitted: (value) {
                  actState.monthUnderInterestInterestCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.taxCESSRInterestCostNode);
                },
                controller: actState.monthUnderInterestCostController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Taxa da CESSR %',
                showAlert: appState.invalidFields.contains(ValidFields.taxCESSR)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.taxCESSR);
                  });
                },
                focusNode: actState.taxCESSRInterestCostNode,
                onSubmitted: (value) {
                  actState.taxCESSRInterestCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.yearlyOperationCostOfTrainingNode);
                },
                controller: actState.taxCESSRCostController),
            Text(
              'Informe abaixo a depreciação da cultura perene',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            const SizedBox(
              height: 15,
            ),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle:
                'Somatório do Custo Operacional Efetivo dos Anos de Formação',
                showAlert: appState.invalidFields.contains(ValidFields.yearlyOperationCostOfTraining)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                inputFormatters: [CurrencyTextInputFormatter(
                    decimalDigits: 2,
                    symbol: 'R\$'
                )],
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.yearlyOperationCostOfTraining);
                  });
                },
                focusNode: actState.yearlyOperationCostOfTrainingNode,
                onSubmitted: (value) {
                  actState.productiveLifeCostNode.unfocus();
                  FocusScope.of(context).requestFocus(actState.productiveLifeCostNode);
                },
                controller: actState.yearlyOperationCostOfTrainingController),
            MyInformationField(
                textInputType: TextInputType.number,
                fieldTitle: 'Vida Produtiva da Cultura (em anos)',
                showAlert: appState.invalidFields.contains(ValidFields.productiveLifeCost)
                    ? true
                    : false,
                alertMessage:
                'Campo obrigatório',
                onTap: (){
                  setState(() {
                    appState.invalidFields.remove(ValidFields.productiveLifeCost);
                  });
                },
                focusNode: actState.productiveLifeCostNode,
                onSubmitted: (value) {
                  actState.productiveLifeCostNode.unfocus();
                },
                controller: actState.productiveLifeCostController),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  label: 'Editar',
                  onPress: () {
                    validateFields(appState,actState,authState,context);
                  },
                ),
              ],
            ),
          ]);
        });
      });
    });
  }

  void validateFields(AppProvider appState, ActivityProvider actState, AuthProvider authState, BuildContext context) {
    appState.invalidFields= [];
    ProductiveActivityResponse payload = ProductiveActivityResponse();
    checkFieldActivity(ValidFields.activityName, appState, actState, actState.activityNameController);
    checkFieldActivity(ValidFields.size, appState,actState, actState.sizeController);
    checkFieldActivity(ValidFields.expectation, appState,actState, actState.expectationController);
    checkFieldActivity(ValidFields.capacity, appState,actState, actState.capacityCodeController);
    checkFieldActivity(ValidFields.insuranceCost, appState,actState, actState.insuranceCostController);
    checkFieldActivity(ValidFields.monthsUnderInterest, appState,actState, actState.monthUnderInterestCostController);
    checkFieldActivity(ValidFields.taxCESSR, appState,actState, actState.taxCESSRCostController);
    checkFieldActivity(ValidFields.technicalAssistanceCost, appState,actState, actState.technicalAssistanceCostController);
    checkFieldActivity(ValidFields.yearlyInterestCost, appState,actState, actState.yearlyInterestCostController);
    checkFieldActivity(ValidFields.yearlyOperationCostOfTraining, appState,actState, actState.yearlyOperationCostOfTrainingController);
    checkFieldActivity(ValidFields.productiveLifeCost, appState,actState, actState.productiveLifeCostController);
    setState(() {
    });

    if (appState.invalidFields.isNotEmpty) {
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }else{
      showDialog(
          context: context,
          builder: (BuildContext context) {
            dialogContext = context;
            return const DefaultAlert(
              text: "Salvando...",
            );
          },
          useSafeArea: true,
          barrierDismissible: false);
      payload = ProductiveActivityResponse(
        activityName:actState.chosenActivityName,
        technology:actState.chosenTecnology,
        pmu:authState.usePmu,
        size:int.parse(checkFieldActivity(ValidFields.size, appState,actState, actState.sizeController)),
        expectation:int.parse(checkFieldActivity(ValidFields.expectation, appState,actState, actState.expectationController)),
        measurementUnit:actState.chosenUnity,
        capacity:checkFieldActivity(ValidFields.capacity, appState,actState, actState.capacityCodeController) == null?1:int.parse(checkFieldActivity(ValidFields.capacity, appState,actState, actState.capacityCodeController)),
        insuranceCost:double.parse(checkFieldActivity(ValidFields.insuranceCost, appState,actState, actState.insuranceCostController).replaceAll(RegExp(r'[^\d.]+'),'')),
        technicalAssistanceCost:double.parse(checkFieldActivity(ValidFields.technicalAssistanceCost, appState,actState, actState.technicalAssistanceCostController).replaceAll(RegExp(r'[^\d.]+'),'')),
        yearlyInterestCost:double.parse(checkFieldActivity(ValidFields.yearlyInterestCost, appState,actState, actState.yearlyInterestCostController)),
        monthsUnderInterest:int.parse(checkFieldActivity(ValidFields.monthsUnderInterest, appState,actState, actState.monthUnderInterestCostController)) ,
        taxCESSR: double.parse(checkFieldActivity(ValidFields.monthsUnderInterest, appState,actState, actState.monthUnderInterestCostController)),
        yearlyOperationCostOfTraining:double.parse(checkFieldActivity(ValidFields.yearlyOperationCostOfTraining, appState,actState, actState.yearlyOperationCostOfTrainingController).replaceAll(RegExp(r'[^\d.]+'),'')),
        productiveLifeCost:int.parse(checkFieldActivity(ValidFields.productiveLifeCost, appState,actState, actState.productiveLifeCostController).replaceAll(RegExp(r'[^\d.]+'),'')),
        id: actState.activityIten.id!,
      );
      ProductiveActivityService().editProductive(payload, authState.token).then((value) {
        ProductiveActivityService().getAllProductive(authState.token).then((value) {
          actState.setLoadedActivities(value);
          Navigator.pop(dialogContext);
          actState.disposeControllers();
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return ActivityMenuPage();
          }));
        });
      }).onError((error, stackTrace) {
        Navigator.pop(context);
        print(error);
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }
}

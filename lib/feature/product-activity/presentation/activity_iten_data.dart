import 'package:flutter/material.dart';
import 'package:production_cost/feature/product-activity/presentation/edit_activity_iten.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/presentation/widgets/my_information_field.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:provider/provider.dart';

import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/providers/activity_provider.dart';
import '../../../shared/providers/app_provider.dart';
import '../data/service/productive_activity_service.dart';
import 'activity_menu_page.dart';

class ActivityItenData extends StatefulWidget {
  const ActivityItenData({Key? key}) : super(key: key);

  @override
  State<ActivityItenData> createState() => _ActivityItenDataState();
}

class _ActivityItenDataState extends State<ActivityItenData> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<ActivityProvider>(builder: (context, actState, child) {
          return FormLayout(
            pageTitle: 'Atividade Produtiva',
              children: [
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Atividade Produtiva',
                    controller: TextEditingController(text: actState.activityIten.activityName!.description)),
            Text('Unidade de Medida da Propriedade', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
            Text(actState.activityIten.pmu!, style: TextStyle(color: AppColor.redColor, fontSize: 15, fontWeight: FontWeight.w700),),
            const SizedBox(height: 25,),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Tamanho da Atividade Produtiva',
                controller: TextEditingController(text: actState.activityIten.size.toString())),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Expectativa de Produção',
                controller: TextEditingController(text: actState.activityIten.expectation.toString())),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Unidade de Medida da Produção',
                controller: TextEditingController(text: actState.activityIten.measurementUnit)),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Capacidade da Medida Escolhida',
                controller: TextEditingController(text: actState.activityIten.size.toString())),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Valor do Seguro - Custo R\$',
                controller: TextEditingController(text: 'R\$ ${actState.activityIten.insuranceCost.toString().replaceAll('.', ',')}')),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Valor da Assistência Técnica - Custo R\$',
                controller: TextEditingController(text: 'R\$ ${actState.activityIten.technicalAssistanceCost.toString().replaceAll('.', ',')}')),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Taxa de Juros %',
                controller: TextEditingController(text: '${actState.activityIten.yearlyInterestCost.toString()} %')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Meses Sobre Juros',
                    controller: TextEditingController(text: '${actState.activityIten.monthsUnderInterest.toString()}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Taxa da CESSR %',
                    controller: TextEditingController(text: '${actState.activityIten.taxCESSR.toString()} %')),
            Text('Informe abaixo a depreciação da cultura perene', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
            const SizedBox(height: 15,),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Somatório do Custo Operacional Efetivo dos Anos de Formação',
                controller: TextEditingController(text: 'R\$ ${actState.activityIten.yearlyOperationCostOfTraining.toString().replaceAll('.', ',')}')),
            MyInformationField(
                disabled: true,
                fieldTitle: 'Vida Produtiva da Cultura (em anos)',
                controller: TextEditingController(text: actState.activityIten.productiveLifeCost.toString())),
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [MyButton(label: 'Voltar',
                onPress: (){
                returnToMenu(appState,authState, actState, context);
                },
              ),],),
            const SizedBox(height: 25,),
            Row(children: [
              Expanded(child: MyButton(label: 'Editar Atividade Produtiva',
              onPress: (){
                editFields(appState,authState,actState,context);
              },))
            ],),
            const SizedBox(height: 25,),
            Row(children: [
              Expanded(child: MyButton(label: 'Deletar Atividade Produtiva',backgroundColor: AppColor.redColor,
              onPress: (){
                removeActivity(appState,authState, actState, context);
              },))
            ],),

          ]);
        });
      });
    });
  }

  void returnToMenu(AppProvider appState, AuthProvider authState, ActivityProvider actState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    ProductiveActivityService().getAllProductive(authState.token).then((value) {
      actState.loadedActivities = value;
      Navigator.pop(dialogContext);
      Navigator.pop(context);
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });

  }

  void removeActivity(AppProvider appState,AuthProvider authState, ActivityProvider actState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    ProductiveActivityService().deleteProductive(actState.activityIten.id!, authState.token).then((value) {
      ProductiveActivityService().getAllProductive(authState.token).then((value) {
        actState.setLoadedActivities(value);
        actState.disposeControllers();
        Navigator.pop(dialogContext);
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return ActivityMenuPage();
        }));
      });
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void openregisterDialog(AppProvider appState,AuthProvider authState,ActivityProvider actState, BuildContext context) {
    Widget okButton = Expanded(child: MyButton(
      width: 200,
      label: 'Retornar ao menu',
      onPress: (){
        Navigator.of(context).pop();
      },
    ));
    AlertDialog alert = AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text(
        'Atividade Produtiva deletada com sucesso!',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
      ),
      actions: [okButton],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void editFields(AppProvider appState, AuthProvider authState, ActivityProvider actState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    actState.chosenActivityName = actState.activityIten.activityName!;
    actState.activityNameController.text = actState.activityIten.activityName!.description!;
    actState.technologyController.text = actState.activityIten.technology!;
    actState.pmuController.text = actState.activityIten.pmu!;
    actState.sizeController.text = actState.activityIten.size!.toString();
    actState.expectationController.text = actState.activityIten.expectation!.toString();
    actState.measurementUnitController.text = actState.activityIten.measurementUnit!.toString();
    actState.capacityCodeController.text = actState.activityIten.capacity == null? '':actState.activityIten.capacity.toString();
    actState.insuranceCostController.text = actState.activityIten.insuranceCost.toString();
    actState.technicalAssistanceCostController.text = actState.activityIten.technicalAssistanceCost.toString();
    actState.yearlyInterestCostController.text = actState.activityIten.yearlyInterestCost.toString();
    actState.monthUnderInterestCostController.text = actState.activityIten.monthsUnderInterest.toString();
    actState.taxCESSRCostController.text = actState.activityIten.taxCESSR.toString();
    actState.yearlyOperationCostOfTrainingController.text = actState.activityIten.yearlyOperationCostOfTraining.toString();
    actState.productiveLifeCostController.text = actState.activityIten.productiveLifeCost.toString();
    ProductiveActivityService().searchProductiveName(authState.token, '').then((value) {
      actState.chosenTecnology = actState.tecnologies.first;
      actState.chosenUnity = actState.unityList.first;
      actState.setActivitNameList(value);
      Navigator.pop(dialogContext);
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return EditActivityItenPage();
    }));
      });

  }
}

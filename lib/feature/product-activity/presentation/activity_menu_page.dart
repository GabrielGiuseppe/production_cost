import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/product-activity/data/service/productive_activity_service.dart';
import 'package:production_cost/feature/product-activity/presentation/activity_iten_data.dart';
import 'package:production_cost/feature/product-activity/presentation/add_activity_iten.dart';
import 'package:production_cost/shared/presentation/pages/menu_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:provider/provider.dart';

import '../../../shared/presentation/widgets/card_iten.dart';
import '../../../shared/presentation/widgets/default_alert.dart';

class ActivityMenuPage extends StatefulWidget {
  const ActivityMenuPage({Key? key}) : super(key: key);

  @override
  State<ActivityMenuPage> createState() => _ActivityMenuPageState();
}

class _ActivityMenuPageState extends State<ActivityMenuPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<ActivityProvider>(builder: (context, actState, child) {
        return Consumer<AuthProvider>(builder: (context, authState, child) {
          return MenuLayout(
              pageTitle: 'Atividade Produtiva',
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: actState.loadedActivities.length,
                        itemBuilder: (context, index) {
                          return CardIten(
                            onTap: () {
                              openItenDesc(appState, actState, index, context);
                            },
                            itenTitle: actState.loadedActivities[index]
                                .activityName!.description!,
                            itenSubtitle: '${actState.loadedActivities[index]
                                .size} ${actState.loadedActivities[index].pmu}',
                            itenDescription: 'Estimativa de produção: ${actState
                                .loadedActivities[index].expectation} ${actState
                                .loadedActivities[index].measurementUnit}(s)',
                          );
                        }),),
                    SizedBox(height: 30,),
                    Row(children: [Expanded(child: MyButton(
                      label: 'Adicionar +',
                      onPress: () {
                        addActivity(appState,authState, actState, context);
                      },
                    ))
                    ],)
                  ],)
              ]);
        });
      });
    });
  }

  void addActivity(AppProvider appState, AuthProvider authState,

      ActivityProvider actState, BuildContext context) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
      actState.disposeControllers();
    ProductiveActivityService().searchProductiveName(authState.token, '').then((value) {
      actState.chosenTecnology = actState.tecnologies.first;
      actState.chosenUnity = actState.unityList.first;
      actState.setActivitNameList(value);
      Navigator.pop(dialogContext);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return AddActivityPage();
    }));
    });
  }

  void openItenDesc(AppProvider appState, ActivityProvider actState, int index,
      BuildContext context) {
    actState.activityIten = actState.loadedActivities[index];
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ActivityItenData();
    }));
  }
}

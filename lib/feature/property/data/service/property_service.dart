import 'dart:convert';
import 'dart:io';

import 'package:production_cost/feature/auth/data/model/property.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:http/http.dart' as http;
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
class PropertyService{
  Future<Property> getProperty( AuthProvider authState) async{
    var response = await call(authState);
    Map<String, dynamic> result = jsonDecode(response.body);
    if(response.statusCode == 200){
      return Property.fromJson(jsonDecode(response.body));
    }else{
      throw Exception(jsonDecode(response.body)['message']);
    }

  }

  call(AuthProvider authState) {
    return http.get(Uri.parse('${AppStrings.host}/user/profile'),
        headers: {HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer ${authState.token}"});
  }
}
import 'dart:convert';
import 'dart:io';

import 'package:production_cost/feature/auth/data/model/property.dart';
import 'package:production_cost/feature/property/data/model/property_response.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:http/http.dart' as http;
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/utils/app_strings.dart';

class UpdatePropertyDervice {
  Future<void> updateProperty(AuthProvider authState, PropertyResponse payload) async {
    var response = await call(authState, payload);
    Map<String, dynamic> sending = jsonDecode(jsonEncode(payload));
    if (response.statusCode == 200) {
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  call(AuthProvider authState, PropertyResponse payload) {
    return http.put(Uri.parse('${AppStrings.host}/user/profile'),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer ${authState.token}"
        },
        body: jsonEncode(payload.toJson()));
  }
}

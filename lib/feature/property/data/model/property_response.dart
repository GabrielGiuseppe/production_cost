class PropertyResponse {
  String? accessProfile;
  String? profession;
  String? fullName;
  int? cpf;
  String? birthDate;
  int? phoneCode;
  int? phone;
  String? possessionCondition;
  String? possessionConditionDesc;
  String? propertyName;
  int? cnpj;
  String? pmu;
  int? area;
  String? uf;
  String? city;
  double? latitude;
  double? longitude;
  String? plusCode;
  String? email;
  bool? biometric;

  PropertyResponse({
     this.accessProfile,
    this.profession,
     this.fullName,
     this.cpf,
     this.birthDate,
     this.phoneCode,
     this.phone,
     this.possessionCondition,
    this.possessionConditionDesc,
     this.propertyName,
     this.cnpj,
     this.pmu,
     this.area,
     this.uf,
     this.city,
     this.latitude,
     this.longitude,
     this.plusCode,
     this.email,
     this.biometric
  });

  Map<String, dynamic> toJson() {
    return {
      "accessProfile": accessProfile,
      "profession": profession,
      "fullName": fullName,
      "cpf": cpf,
      "birthDate": birthDate,
      "phoneCode": phoneCode,
      "phone": phone,
      "possessionCondition": possessionCondition,
      "possessionConditionDesc": possessionConditionDesc,
      "propertyName": propertyName,
      "cnpj": cnpj,
      "pmu": pmu,
      "area": area,
      "uf": uf,
      "latitude": latitude,
      "longitude": longitude,
      "city": city,
      "plusCode": plusCode,
      "email": email,
      "biometric": biometric
    };
  }

  factory PropertyResponse.fromJson(Map<String, dynamic> json) {
    return PropertyResponse(
        accessProfile: json['accessProfile'],
        profession: json['profession'],
        fullName: json['fullName'],
        cpf: json['cpf'],
        birthDate: json['birthDate'],
        phoneCode: json['phoneCode'],
        phone: json['phone'],
        possessionCondition: json['possessionCondition'],
        possessionConditionDesc: json['possessionConditionDesc'],
        propertyName: json['propertyName'],
        cnpj: json['cnpj'],
        pmu: json['pmu'],
        area: json['area'],
        uf: json['uf'],
        city:  json['city'],
        latitude:  double.parse(json['latitude'].toString()),
        longitude:  double.parse(json['longitude'].toString()),
        plusCode: json['plusCode'],
        email: json['email'],
        biometric: json['biometric']
    );
  }
}

import 'package:flutter/material.dart';
import 'package:production_cost/feature/dashboard_page.dart';
import 'package:production_cost/feature/property/data/model/property_response.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/providers/property_provider.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_auto_complete_city_field.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/presentation/widgets/my_date_picker_field.dart';
import '../../../shared/presentation/widgets/my_dropdown_field.dart';
import '../../../shared/presentation/widgets/my_information_field.dart';
import '../../../shared/presentation/widgets/text_switch.dart';
import '../../../shared/utils/input_masks.dart';
import '../../../shared/utils/register_fild_validator.dart';
import '../../../shared/utils/string_utils.dart';
import '../../auth/data/model/property.dart';
import '../data/service/update_property_service.dart';
class PropertyDataPage extends StatefulWidget {
  const PropertyDataPage({Key? key}) : super(key: key);

  @override
  State<PropertyDataPage> createState() => _PropertyDataPageState();
}

class _PropertyDataPageState extends State<PropertyDataPage> {

  late BuildContext dialogContext;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<PropertyProvider>(builder: (context, propState, child) {
          return FormLayout(
              pageTitle: 'Editar Propriedade',
              children: [
                MyDropDownField(
                  initialValue: appState.accessProfileValue,
                  valuesList: appState.accessProfileList,
                  fieldTitle: 'Perfil de Acesso',
                  fieldType: FieldType.profission,
                ),
                appState.isProfissional
                    ? MyInformationField(
                  showAlert: appState.invalidFields.contains(ValidFields
                      .profession)
                      ? true
                      : false,
                  alertMessage:
                  'Ao escolher profissional este campo deve ser preenchido',
                  controller: propState.professionController,
                  fieldTitle: 'Profissão',
                  focusNode: propState.professionNode,
                  onSubmitted: (value) {
                    propState.professionNode.unfocus();
                    FocusScope.of(context).requestFocus(propState.nameNode);
                  },
                  onTap: () {
                    setState(() {
                      appState.invalidFields.remove(ValidFields.profession);
                    });
                  },
                )
                    : const SizedBox(),
                MyInformationField(
                    controller: propState.nameController,
                    fieldTitle: 'Nome Completo',
                    focusNode: propState.nameNode,
                    showAlert: appState.invalidFields.contains(
                        ValidFields.fullName)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(ValidFields.fullName);
                      });
                    },
                    onSubmitted: (value) {
                      propState.nameNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.cpfNode);
                    }),
                MyInformationField(
                    controller: propState.cpfController,
                    fieldTitle: 'CPF',
                    inputFormatters: [InputMasks.cpfMask()],
                    textInputType: TextInputType.number,
                    focusNode: propState.cpfNode,
                    showAlert: appState.invalidFields.contains(
                        ValidFields.cpf)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(ValidFields.cpf);
                      });
                    },
                    onSubmitted: (value) {
                      propState.cpfNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.dateNode);
                    }),
                MyDatePickerField(
                    controller: propState.dateController,
                    focusNode: propState.dateNode,
                    nextNode: propState.dddNode,
                    keyboardType: TextInputType.number,
                    fieldTitle: 'Data de Nascimento',
                    inputFormatters: [InputMasks.dateMask()],
                    showAlert: appState.invalidFields.contains(
                        ValidFields.birthDate)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(ValidFields.birthDate);
                      });
                    },
                    onPress: () async {
                      DateTime initialTime = DateTime.now();
                      if (propState.dateController.text.isNotEmpty) {
                        initialTime = StringUtils().convertStringToDate(
                            propState.dateController.text, "dd/MM/yyyy");
                      }

                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: initialTime,
                          firstDate: DateTime(1800),
                          lastDate: DateTime.now());

                      if (pickedDate != null) {
                        String formattedDate = DateFormat("dd/MM/yyyy").format(
                            pickedDate);
                        setState(() {
                          propState.dateController.text = formattedDate;
                        });
                      }
                    },
                    onSubmitted: (value) {
                      propState.dateNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.dddNode);
                    }
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: MyInformationField(
                        controller: propState.dddController,
                        width: 70,
                        fieldTitle: 'DDD',
                        inputFormatters: [InputMasks.dddMask()],
                        textInputType: TextInputType.number,
                        focusNode: propState.dddNode,
                        showAlert: appState.invalidFields.contains(
                            ValidFields.phoneCode) ||
                            appState.invalidFields.contains(
                                ValidFields.phone)
                            ? true
                            : false,
                        alertMessage:
                        '*',
                        onSubmitted: (value) {
                          propState.dddNode.unfocus();
                          FocusScope.of(context).requestFocus(
                              propState.phoneNode);
                        },
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        flex: 4,
                        child: MyInformationField(
                          controller: propState.phoneController,
                          fieldTitle: 'Telefone',
                          inputFormatters: [InputMasks.phoneMask()],
                          textInputType: TextInputType.number,
                          focusNode: propState.phoneNode,
                          showAlert: appState.invalidFields.contains(
                              ValidFields.phoneCode) ||
                              appState.invalidFields.contains(
                                  ValidFields.phone)
                              ? true
                              : false,
                          alertMessage:
                          'Campo obrigatório',
                          onTap: () {
                            setState(() {
                              appState.invalidFields.remove(
                                  ValidFields.phone);
                              appState.invalidFields.remove(
                                  ValidFields.phoneCode);
                            });
                          },
                        ))
                  ],
                ),
                MyDropDownField(
                  initialValue: appState.possessionConditionValue,
                  valuesList: appState.possessionConditionlist,
                  fieldTitle: 'Condição da Posse',
                  fieldType: FieldType.possession,
                ),
                appState.isOthers
                    ? MyInformationField(
                    controller: propState.possessionController,
                    fieldTitle: 'Descreva a Condição de Posse',
                    focusNode: propState.possessionNode,
                    showAlert: appState.invalidFields.contains(ValidFields
                        .possessionConditionDesc)
                        ? true
                        : false,
                    alertMessage:
                    'Ao escolher Outros este campo deve ser preenchido',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(
                            ValidFields.possessionConditionDesc);
                      });
                    },
                    onSubmitted: (value) {
                      propState.possessionNode.unfocus();
                      FocusScope.of(context).requestFocus(
                          propState.propertyNameNode);
                    })
                    : const SizedBox(),
                MyInformationField(
                    controller: propState.propertyNameController,
                    fieldTitle: 'Nome da Propriedade',
                    focusNode: propState.propertyNameNode,
                    showAlert: appState.invalidFields.contains(
                        ValidFields.propertyName)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(
                            ValidFields.propertyName);
                      });
                    },
                    onSubmitted: (value) {
                      propState.propertyNameNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.cnpjNode);
                    }),
                MyInformationField(
                  controller: propState.cnpjController,
                  fieldTitle: 'CNPJ da Propriedade',
                  showAlert: appState.invalidFields.contains(
                      ValidFields.cnpj)
                      ? true
                      : false,
                  alertMessage:
                  'Campo obrigatório',
                  onTap: () {
                    setState(() {
                      appState.invalidFields.remove(ValidFields.cnpj);
                    });
                  },
                  inputFormatters: [InputMasks.cnpjMask()],
                  textInputType: TextInputType.number,
                  focusNode: propState.cnpjNode,
                ),
                MyDropDownField(
                  valuesList: appState.unityList,
                  initialValue: appState.unityValue,
                  fieldTitle: 'Unidade de Medida da Propriedade',
                ),
                MyInformationField(
                  controller: propState.propertieSizeController,
                  fieldTitle: 'Tamanho da área da propriedade',
                  showAlert: appState.invalidFields.contains(
                      ValidFields.area)
                      ? true
                      : false,
                  alertMessage:
                  'Campo obrigatório',
                  onTap: () {
                    setState(() {
                      appState.invalidFields.remove(ValidFields.area);
                    });
                  },
                  focusNode: propState.propertieSizeNode,
                  textInputType: TextInputType.number,
                ),
                MyDropDownField(
                  valuesList: appState.ufList,
                  initialValue: appState.ufValue,
                  fieldType: FieldType.uf,
                  fieldTitle: 'Selecione a UF',
                ),
                MyAutoCompleteCityField(
                  prefillText: propState.cityController.text,
                    fieldTitle: 'Digite e Selecione o Município',
                    onSubmitted: (value) {
                      FocusScope.of(context).requestFocus(
                          propState.plusCodeNode);
                    }
                ),
                Row(
                  children: [
                    Text('Localização da propriedade',
                      style: TextStyle(fontSize: 15,
                          color: appState.darkMode
                              ? AppColor.secondaryBlue
                              : AppColor.secondaryBlueLight,
                          fontWeight: FontWeight.w700),),
                    SizedBox(width: 10,),
                    Expanded(child: MyButton(
                      onPress: (){
                        relocate(appState, propState, context);
                      },
                      fontSize: 15,
                      label: 'Relocalizar',
                      height: 30,
                    ))
                  ],
                ),
                const SizedBox(height: 25,),
                MyInformationField(
                    disabled: true,
                    controller: appState.latController,
                    fieldTitle: 'Latitude',
                    focusNode: propState.latNode,
                    onSubmitted: (value) {
                      propState.latNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.longNode);
                    }),
                MyInformationField(
                    disabled: true,
                    controller: appState.longController,
                    fieldTitle: 'Logitude',
                    focusNode: propState.longNode,
                    onSubmitted: (value) {
                      propState.longNode.unfocus();
                      FocusScope.of(context).requestFocus(
                          propState.plusCodeNode);
                    }),
                MyInformationField(
                    controller: propState.plusCodeController,
                    fieldTitle: 'Informe o Plus Code da propriedade',
                    focusNode: propState.plusCodeNode,
                    onSubmitted: (value) {
                      propState.plusCodeNode.unfocus();
                      FocusScope.of(context).requestFocus(propState.emailNode);
                    }),
                MyInformationField(
                    textCapitalization: TextCapitalization.none,
                    controller: propState.emailController,
                    fieldTitle: 'Informe o seu e-mail',
                    showAlert: appState.invalidFields.contains(
                        ValidFields.email)
                        ? true
                        : false,
                    alertMessage:
                    'Campo obrigatório',
                    onTap: () {
                      setState(() {
                        appState.invalidFields.remove(ValidFields.email);
                      });
                    },
                    focusNode: propState.emailNode,
                    onSubmitted: (value) {
                      propState.emailNode.unfocus();
                    }),
                TextSwitch(
                    onChanged: (bool value) {
                      setState(() {
                        propState.isBiometric = value;
                      });
                    },
                    label: "Solicitar Biometria ou Face ID",
                    initialValue: propState.isBiometric),
                Row(
                  children: [
                    Expanded(
                        child: MyButton(
                          label: 'Salvar',
                          fontSize: 25,
                          onPress: () {
                            validateFields(
                                appState, propState, authState, context);
                          },
                        ))
                  ],
                )
              ]);
        });
      });
    });
  }

  void validateFields(AppProvider appState, PropertyProvider propState,
      AuthProvider authState, BuildContext context) {
    PropertyResponse payload = PropertyResponse();
    appState.invalidFields = [];
    appState.accessProfileValue;
    checkField(
        ValidFields.profession, appState, propState.professionController);
    checkField(ValidFields.fullName, appState, propState.nameController);
    checkField(ValidFields.cpf, appState, propState.cpfController);
    checkField(ValidFields.birthDate, appState, propState.dateController);
    checkField(ValidFields.phoneCode, appState, propState.dddController);
    checkField(ValidFields.phone, appState, propState.phoneController);
    appState.possessionConditionValue;
    checkField(ValidFields.possessionConditionDesc, appState,
        propState.possessionController);
    checkField(ValidFields.propertyName, appState,
        propState.propertyNameController);
    checkField(ValidFields.cnpj, appState, propState.cnpjController);
    checkField(
        ValidFields.area, appState, propState.propertieSizeController);
    checkField(ValidFields.city, appState, TextEditingController());
    checkField(ValidFields.email, appState, propState.emailController);

    if (appState.invalidFields.isNotEmpty) {
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      payload = PropertyResponse(
          accessProfile: appState.accessProfileValue,
          profession: checkField(ValidFields.profession, appState,
              propState.professionController),
          fullName: checkField(
              ValidFields.fullName, appState, propState.nameController),
          cpf: int.parse(checkField(
              ValidFields.cpf, appState, propState.cpfController)),
          birthDate: dateMask(checkField(
              ValidFields.birthDate, appState, propState.dateController)),
          phoneCode: int.parse(checkField(
              ValidFields.phoneCode, appState, propState.dddController)),
          phone: int.parse(checkField(
              ValidFields.phone, appState, propState.phoneController)),
          possessionCondition: appState.possessionConditionValue,
          possessionConditionDesc: checkField(
              ValidFields.possessionConditionDesc, appState,
              propState.possessionController),
          propertyName: checkField(ValidFields.propertyName, appState,
              propState.propertyNameController),
          cnpj: int.parse(checkField(
              ValidFields.cnpj, appState, propState.cnpjController)),
          pmu: appState.unityValue,
          area: int.parse(checkField(ValidFields.area, appState,
              propState.propertieSizeController)),
          uf: appState.ufValue,
          city: checkField(
              ValidFields.city, appState, TextEditingController()),
          latitude: double.parse(checkField(
              ValidFields.latitude, appState, appState.latController)),
          longitude: double.parse(checkField(
              ValidFields.longitude, appState, appState.longController)),
          plusCode: propState.plusCodeController.text == "" ? null : propState
              .plusCodeController.text,
          email: checkField(
              ValidFields.email, appState, propState.emailController),
          biometric: propState.isBiometric);
      setState(() {});
      updateUser(appState, payload, authState, context);
    }
  }

  void updateUser(AppProvider appState, PropertyResponse payload,
      AuthProvider authState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Salvando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    UpdatePropertyDervice().updateProperty(authState, payload).then((value) {
      Navigator.pop(dialogContext);
      openregisterDialog(appState, context);
    }).onError((error, stackTrace) {
      Navigator.pop(context);
      print(error);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  void openregisterDialog(AppProvider appState, BuildContext context) {
    Widget okButton = Expanded(child: MyButton(
      width: 200,
      label: 'Retornar ao menu',
      onPress: () {
        Navigator.of(context).pop();
        Navigator.pushAndRemoveUntil(
            context, MaterialPageRoute(builder: (context) {
          return const DashboradPage();
        }), (route) => false);
      },
    ));
    AlertDialog alert = AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text(
        'Dados salvos com sucesso!',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
      ),
      actions: [okButton],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void relocate(AppProvider appState, PropertyProvider propState, BuildContext context) {
    late BuildContext dialogContext;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Obtendo localização...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    appState.getLocation().then((value) {
      appState.lat = value.latitude;
      appState.long = value.longitude;
      appState.latController.text = appState.lat.toString();
      appState.longController.text = appState.long.toString();
      Navigator.pop(dialogContext);
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
    });
  }
}
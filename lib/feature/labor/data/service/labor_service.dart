import 'dart:async';
import 'dart:convert';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:http/http.dart' as http;

import '../model/labor_payload.dart';
import '../model/labor_response.dart';

class LaborService {
  String host = AppStrings.host;

  Future<dynamic> add(LaborPayload payload, String token) async {
    final response = await http.post(
        Uri.parse(
            '$host/labors'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));

    print(response.body);
    if (response.statusCode == 201) {
      return LaborResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<LaborResponse> edit(LaborResponse payload, String token) async {
    final response = await http.put(
        Uri.parse(
            '$host/labors'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        },
        body: jsonEncode(payload));

    if (response.statusCode == 200) {
      return LaborResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> delete(String id, String token) async {
    final response = await http.delete(
        Uri.parse(
            '$host/labors/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    print(id);
    if (response.statusCode == 200) {
      return "success";
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<dynamic> get(String id, String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/labors/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    if (response.statusCode == 200) {
      return LaborResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }

  Future<List<LaborResponse>> getAll(String token) async {
    final response = await http.get(
        Uri.parse(
            '$host/labors'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization' : 'Bearer $token'
        });

    String responsePayload = response.body.toString();
    if (response.statusCode == 200) {
      List<LaborResponse> listProductive = (json.decode(response.body) as List)
          .map((data) => LaborResponse.fromJson(data))
          .toList();
      return listProductive;
    } else {
      throw Exception(jsonDecode(response.body)['message']);
    }
  }
}

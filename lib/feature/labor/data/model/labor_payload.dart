

import '../../../machianry/data/model/other_cost_response.dart';
import '../../../product-activity/data/model/productive_activity_response.dart';

class LaborPayload {
  ProductiveActivityResponse? production;
  String? commonLaborCostType;
  double? commonLaborCost;
  String? laborCostType;
  double? driverCost;
  double? tractorDriverCost;
  double? bleederCost;
  double? taxCost;
  double? technicalAssistanceCost;
  double? transportCost;
  List<OtherCostResponse>? otherCost;

  LaborPayload({
    this.production,
    this.commonLaborCostType,
    this.commonLaborCost,
    this.laborCostType,
    this.driverCost,
    this.tractorDriverCost,
    this.bleederCost,
    this.taxCost,
    this.technicalAssistanceCost,
    this.transportCost,
    this.otherCost,
  });

  Map<String, dynamic> toJson() {
    return {
      "productiveActivity": production,
      "commonLaborCostType": commonLaborCostType,
      "commonLaborCost": commonLaborCost,
      "laborCostType": laborCostType,
      "motorDriverCost":driverCost,
      "tractorDriverCost": tractorDriverCost,
      "bleederCost": bleederCost,
      "taxCost": taxCost,
      "technicalAssistanceCost": technicalAssistanceCost,
      "transportCost": taxCost,
      "otherCost" : otherCost
    };
  }
}

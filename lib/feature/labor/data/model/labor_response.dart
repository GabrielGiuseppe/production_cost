import '../../../machianry/data/model/other_cost_response.dart';
import '../../../product-activity/data/model/productive_activity_response.dart';

class LaborResponse{
  ProductiveActivityResponse? production;
  String? id;
  String? commonLaborCostType;
  double? commonLaborCost;
  String? laborCostType;
  double? driverCost;
  double? tractorDriverCost;
  double? bleederCost;
  double? taxCost;
  double? technicalAssistanceCost;
  double? transportCost;
  List<OtherCostResponse>? otherCost;

  LaborResponse({
     this.production,
     this.id,
     this.commonLaborCostType,
     this.commonLaborCost,
     this.laborCostType,
    this.driverCost,
     this.tractorDriverCost,
     this.bleederCost,
     this.taxCost,
     this.technicalAssistanceCost,
     this.transportCost,
     this.otherCost,
  });

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "productiveActivity" : production,
      "commonLaborCostType": commonLaborCostType,
      "commonLaborCost": commonLaborCost,
      "laborCostType": laborCostType,
      "motorDriverCost":driverCost,
      "tractorDriverCost": tractorDriverCost,
      "bleederCost": bleederCost,
      "taxCost": taxCost,
      "technicalAssistanceCost": technicalAssistanceCost,
      "transportCost": transportCost,
      "otherCost": otherCost
    };
  }

  factory LaborResponse.fromJson(Map<String, dynamic> json) {
    var list = json['otherCost'] as List;
    List<OtherCostResponse> otherCost = list.map((i) => OtherCostResponse.fromJson(i)).toList();
    return LaborResponse(
      production: ProductiveActivityResponse.fromJson(json['productiveActivity']),
      id: json['id'],
      commonLaborCostType: json['commonLaborCostType'],
      commonLaborCost: double.parse(json['commonLaborCost'].toString()),
      laborCostType: json['laborCostType'],
      driverCost: double.parse(json['motorDriverCost'].toString()),
      tractorDriverCost: double.parse(json['tractorDriverCost'].toString()),
      bleederCost: double.parse(json['bleederCost'].toString()),
      taxCost: double.parse(json['taxCost'].toString()),
      technicalAssistanceCost: double.parse(json['technicalAssistanceCost'].toString()),
      transportCost: double.parse(json['transportCost'].toString()),
      otherCost: otherCost,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:production_cost/feature/labor/presentation/labor_add_iten.dart';
import 'package:production_cost/feature/labor/presentation/labor_iten_data.dart';
import 'package:production_cost/feature/product-activity/data/service/productive_activity_service.dart';
import 'package:production_cost/shared/presentation/pages/menu_layout.dart';
import 'package:production_cost/shared/presentation/widgets/labeled_radio.dart';
import 'package:production_cost/shared/providers/labor_provider.dart';
import 'package:provider/provider.dart';

import '../../../shared/presentation/widgets/card_iten.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
class LaborMenuPage extends StatefulWidget {
  const LaborMenuPage({Key? key}) : super(key: key);

  @override
  State<LaborMenuPage> createState() => _LaborMenuPageState();
}

class _LaborMenuPageState extends State<LaborMenuPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<LaborProvider>(builder: (context, labState, child) {
        return Consumer<AuthProvider>(builder: (context, authState, child) {
          return MenuLayout(
            pageTitle: 'Mão de Obra',
              children: [
                Container(child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: labState.loadedItens.length,
                    itemBuilder: (context, index) {
                      return CardIten(
                        onTap: () {
                          openItenDesc(appState,authState, labState, index, context);
                        },
                        itenTitle: labState.loadedItens[index].production!.activityName!.description!,
                        itenSubtitle: '',
                        itenDescription: '',
                        isLonelyInformation: true,
                      );
                    }),),
                SizedBox(height: 30,),
                Row(children: [Expanded(child: MyButton(
                  label: 'Adicionar +',
                  onPress: () {
                    addActivity(appState,authState, labState, context);
                  },
                ))
                ],)
              ]);
        });});});
  }

  void openItenDesc(AppProvider appState, AuthProvider authState, LaborProvider labState, int index, BuildContext context) {
    labState.laborIten = labState.loadedItens[index];
    labState.commonLaborCostType = labState.laborIten.commonLaborCostType =='daily'? CommonLabor.valorDiaria:CommonLabor.mensal;
    labState.laborCostType = labState.laborIten.laborCostType =='daily'? CommonLabor.valorDiaria:CommonLabor.mensal;
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return LaborItenDataPage();
    }));
  }

  void addActivity(AppProvider appState, AuthProvider authState, LaborProvider labState, BuildContext context) {
    labState.disposeControllers();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    ProductiveActivityService().getAllProductive(authState.token).then((value) {
      labState.setActivity(value);
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return LaborAddItenPage();
      }));
    });

  }
}

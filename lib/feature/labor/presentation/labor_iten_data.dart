import 'package:flutter/material.dart';
import 'package:production_cost/feature/labor/data/service/labor_service.dart';
import 'package:production_cost/feature/labor/presentation/labor_edit_iten.dart';
import 'package:production_cost/feature/labor/presentation/labor_menu_page.dart';
import 'package:production_cost/shared/presentation/pages/form_layout.dart';
import 'package:production_cost/shared/providers/labor_provider.dart';
import 'package:provider/provider.dart';

import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/labeled_radio.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/presentation/widgets/my_information_field.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import '../../product-activity/data/service/productive_activity_service.dart';
import '../data/model/labor_response.dart';

class LaborItenDataPage extends StatefulWidget {
  const LaborItenDataPage({Key? key}) : super(key: key);

  @override
  State<LaborItenDataPage> createState() => _LaborItenDataPageState();
}

class _LaborItenDataPageState extends State<LaborItenDataPage> {
  late BuildContext dialogContext;
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<AuthProvider>(builder: (context, authState, child) {
        return Consumer<LaborProvider>(builder: (context, labState, child) {
          return FormLayout(
            pageTitle: 'Detalhes da Mão de Obra',
              children: [
                MyInformationField(disabled: true,controller: TextEditingController(text: labState.laborIten.production!.activityName!.description!),
                  fieldTitle: 'Atividade Produtiva',),
                Text('Unidade de Medida da Propriedade', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
                Text(authState.usePmu, style: TextStyle(color: AppColor.redColor, fontSize: 15, fontWeight: FontWeight.w700),),
                const SizedBox(height: 25,),
                Text('Custo da Mão de Obra Comum', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LabeledRadio(
                      label: 'Valor Diária',
                      padding: EdgeInsets.zero,
                      groupValue: labState.commonLaborCostType,
                      value: CommonLabor.valorDiaria,
                      onChanged: (CommonLabor newValue) {
                      },
                    ),
                    LabeledRadio(
                      label: 'Mensal',
                      padding: EdgeInsets.zero,
                      groupValue: labState.commonLaborCostType,
                      value: CommonLabor.mensal,
                      onChanged: (CommonLabor newValue) {
                      },
                    ),
                  ],
                ),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Mão de Obra Comum (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.commonLaborCost.toString().replaceAll('.', ',')}')),
                Text('Custos abaixo informado em:', style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LabeledRadio(
                      label: 'Valor Diária',
                      padding: EdgeInsets.zero,
                      groupValue: labState.laborCostType,
                      value: CommonLabor.valorDiaria,
                      onChanged: (CommonLabor newValue) {
                      },
                    ),
                    LabeledRadio(
                      label: 'Mensal',
                      padding: EdgeInsets.zero,
                      groupValue: labState.laborCostType,
                      value: CommonLabor.mensal,
                      onChanged: (CommonLabor newValue) {
                      },
                    ),
                  ],
                ),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Motorista (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.driverCost.toString().replaceAll('.', ',')}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Tratorista (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.tractorDriverCost.toString().replaceAll('.', ',')}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Sangrador (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.bleederCost.toString().replaceAll('.', ',')}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Fiscal (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.taxCost.toString().replaceAll('.', ',')}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Assistência Técnica (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.technicalAssistanceCost.toString().replaceAll('.', ',')}')),
                MyInformationField(
                    disabled: true,
                    fieldTitle: 'Gasto com Transporte (Custo R\$)',
                    controller: TextEditingController(text: 'R\$ ${labState.laborIten.transportCost.toString().replaceAll('.', ',')}')),
                Divider(thickness: 2,),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: labState.laborIten.otherCost!.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Iten: ${labState.laborIten.otherCost![index].description}',style: TextStyle(fontSize: 18, color: appState.darkMode?Colors.white:AppColor.font),),
                            Text('Custo: R\$ ${labState.laborIten.otherCost![index].value}', style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),),
                            const SizedBox(height: 15,),
                            Divider(thickness: 1,)
                          ],
                        ),
                      );
                    }),
                const SizedBox(height: 25,),
                Row(mainAxisAlignment: MainAxisAlignment.center,
                  children: [MyButton(label: 'Voltar',
                    onPress: (){
                      returnToMenu(appState,authState, labState, context);
                    },
                  ),],),
                const SizedBox(height: 25,),
                Row(children: [
                  Expanded(child: MyButton(label: 'Editar Maquinário',
                    onPress: (){
                      editFields(appState,authState,labState,context);
                    },))
                ],),
                const SizedBox(height: 25,),
                Row(children: [
                  Expanded(child: MyButton(label: 'Deletar Maquinário',backgroundColor: AppColor.redColor,
                    onPress: (){
                      removeActivity(appState,authState, labState, context);
                    },))
                ],),
              ]);
        });
      });
    });
  }

  void removeActivity(AppProvider appState, AuthProvider authState, LaborProvider labState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    LaborService().delete(labState.laborIten.id!, authState.token).then((value) {
      LaborService().getAll(authState.token).then((value) {
        labState.loadedItens = value;
        ProductiveActivityService().getAllProductive(authState.token).then((value) {
          for(LaborResponse labor in labState.loadedItens) {
            labor.production =
                value.firstWhere((element) => labor.production!.id ==
                    element.id);
          }
            Navigator.pop(dialogContext);
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return LaborMenuPage();
        }));
      });
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });});
  }

  void editFields(AppProvider appState, AuthProvider authState, LaborProvider labState, BuildContext context) {
    labState.disposeControllers();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    ProductiveActivityService().getAllProductive(authState.token).then((value) {
      fillFields(labState);
      labState.setActivity(value);
      Navigator.pop(dialogContext);
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return LaborEditItenPage();
      }));
    });
  }

  void returnToMenu(AppProvider appState, AuthProvider authState, LaborProvider labState, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Carregando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    LaborService().getAll(authState.token).then((value) {
      labState.loadedItens = value;
      ProductiveActivityService().getAllProductive(authState.token).then((value) {
        for(LaborResponse labor in labState.loadedItens) {
          labor.production =
              value.firstWhere((element) => labor.production!.id ==
                  element.id);
        }
      Navigator.pop(dialogContext);
      Navigator.pop(context);
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: Text(
          error.toString().replaceAll('Exception:', ''),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });});
  }

  void fillFields(LaborProvider labState) {
    labState.activityNameController.text = labState.laborIten.production!.activityName!.description!;
    labState.commonLaborCostController.text = labState.laborIten.commonLaborCost.toString();
    labState.driverCostController.text = labState.laborIten.driverCost.toString();
    labState.tractorDriverCostController.text = labState.laborIten.transportCost.toString();
    labState.bleederCostController.text = labState.laborIten.bleederCost.toString();
    labState.taxCostController.text = labState.laborIten.taxCost.toString();
    labState.technicalAssistanceCostController.text = labState.laborIten.technicalAssistanceCost.toString();
    labState.transportCostController.text = labState.laborIten.transportCost.toString();
    labState.otherCustsList = labState.laborIten.otherCost!;
    labState.commonLaborCostType = labState.laborIten.commonLaborCostType == 'daily'? CommonLabor.valorDiaria: CommonLabor.mensal;
    labState.laborCostType = labState.laborIten.laborCostType == 'daily'? CommonLabor.valorDiaria: CommonLabor.mensal;
  }
}

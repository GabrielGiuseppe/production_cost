import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../shared/constants/app_colors.dart';
import '../../../shared/presentation/pages/form_layout.dart';
import '../../../shared/presentation/widgets/default_alert.dart';
import '../../../shared/presentation/widgets/labeled_radio.dart';
import '../../../shared/presentation/widgets/my_button.dart';
import '../../../shared/presentation/widgets/my_dropdown_field.dart';
import '../../../shared/presentation/widgets/my_information_field.dart';
import '../../../shared/providers/app_provider.dart';
import '../../../shared/providers/auth_provider.dart';
import '../../../shared/providers/labor_provider.dart';
import '../../../shared/utils/register_fild_validator.dart';
import '../../machianry/data/model/other_cost_response.dart';
import '../../product-activity/data/model/productive_activity_response.dart';
import '../../product-activity/data/service/productive_activity_service.dart';
import '../data/model/labor_payload.dart';
import '../data/model/labor_response.dart';
import '../data/service/labor_service.dart';
import 'labor_menu_page.dart';
class LaborEditItenPage extends StatefulWidget {
  const LaborEditItenPage({Key? key}) : super(key: key);

  @override
  State<LaborEditItenPage> createState() => _LaborEditItenPageState();
}

class _LaborEditItenPageState extends State<LaborEditItenPage> {
  late BuildContext dialogContext;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<LaborProvider>(builder: (context, labState, child) {
        return Consumer<AuthProvider>(builder: (context, authState, child) {
          return FormLayout(pageTitle: 'Adicionar Mão de Obra', children: [
            MyDropDownField(
              fieldType: FieldType.fuel,
              valuesList: labState.allActivities,
              initialValue: labState.chosenActivity,
              fieldTitle: 'Atividade Produtiva',
            ),
            Text(
              'Unidade de Medida da Propriedade',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            Text(
              authState.usePmu,
              style: TextStyle(
                  color: AppColor.redColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 25,
            ),
            Text(
              'Custo da Mão de Obra Comum',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LabeledRadio(
                  label: 'Valor Diária',
                  padding: EdgeInsets.zero,
                  groupValue: labState.commonLaborCostType,
                  value: CommonLabor.valorDiaria,
                  onChanged: (CommonLabor newValue) {
                    setState(() {
                      labState.commonLaborCostType = newValue;
                    });
                  },
                ),
                LabeledRadio(
                  label: 'Mensal',
                  padding: EdgeInsets.zero,
                  groupValue: labState.commonLaborCostType,
                  value: CommonLabor.mensal,
                  onChanged: (CommonLabor newValue) {
                    setState(() {
                      labState.commonLaborCostType = newValue;
                    });
                  },
                ),
              ],
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.commonLaborCostController,
              fieldTitle: 'Mão de Obra Comum (Custo R\$)',
              focusNode: labState.commonLaborCostFocusNode,
              onSubmitted: (value) {
                labState.commonLaborCostFocusNode.unfocus();
                FocusScope.of(context)
                    .requestFocus(labState.driverCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.commonLaborCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.commonLaborCost);
                });
              },
            ),
            Text(
              'Custos abaixo informado em:',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LabeledRadio(
                  label: 'Valor Diária',
                  padding: EdgeInsets.zero,
                  groupValue: labState.laborCostType,
                  value: CommonLabor.valorDiaria,
                  onChanged: (CommonLabor newValue) {
                    setState(() {
                      labState.laborCostType = newValue;
                    });
                  },
                ),
                LabeledRadio(
                  label: 'Mensal',
                  padding: EdgeInsets.zero,
                  groupValue: labState.laborCostType,
                  value: CommonLabor.mensal,
                  onChanged: (CommonLabor newValue) {
                    setState(() {
                      labState.laborCostType = newValue;
                    });
                  },
                ),
              ],
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.driverCostController,
              fieldTitle: 'Motorista (Custo R\$)',
              focusNode: labState.driverCostFocusNode,
              onSubmitted: (value) {
                labState.driverCostFocusNode.unfocus();
                FocusScope.of(context)
                    .requestFocus(labState.tractorDriverCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.driverCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.driverCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.tractorDriverCostController,
              fieldTitle: 'Tratorista (Custo R\$)',
              focusNode: labState.tractorDriverCostFocusNode,
              onSubmitted: (value) {
                labState.tractorDriverCostFocusNode.unfocus();
                FocusScope.of(context)
                    .requestFocus(labState.bleederCostFocusNode);
              },
              showAlert:
              appState.invalidFields.contains(ValidFields.tractorDriverCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.tractorDriverCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.bleederCostController,
              fieldTitle: 'Sangrador (Custo R\$)',
              focusNode: labState.bleederCostFocusNode,
              onSubmitted: (value) {
                labState.bleederCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(labState.taxCostFocusNode);
              },
              showAlert:
              appState.invalidFields.contains(ValidFields.bleederCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.bleederCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.taxCostController,
              fieldTitle: 'Fiscal (Custo R\$)',
              focusNode: labState.taxCostFocusNode,
              onSubmitted: (value) {
                labState.taxCostFocusNode.unfocus();
                FocusScope.of(context)
                    .requestFocus(labState.technicalAssistanceCostFocusNode);
              },
              showAlert: appState.invalidFields.contains(ValidFields.taxCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.taxCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.technicalAssistanceCostController,
              fieldTitle: 'Assistência Técnica (Custo R\$)',
              focusNode: labState.technicalAssistanceCostFocusNode,
              onSubmitted: (value) {
                labState.technicalAssistanceCostFocusNode.unfocus();
                FocusScope.of(context)
                    .requestFocus(labState.transportCostFocusNode);
              },
              showAlert: appState.invalidFields
                  .contains(ValidFields.laborTechnicalAssistanceCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields
                      .remove(ValidFields.laborTechnicalAssistanceCost);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.transportCostController,
              fieldTitle: 'Gasto com Transporte (Custo R\$)',
              focusNode: labState.transportCostFocusNode,
              onSubmitted: (value) {
                labState.transportCostFocusNode.unfocus();
                FocusScope.of(context).requestFocus(labState.descFocusNode);
              },
              showAlert:
              appState.invalidFields.contains(ValidFields.transportCost)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.transportCost);
                });
              },
            ),
            Text(
              'Informe outra mão de obra',
              style: TextStyle(
                  color: appState.darkMode
                      ? AppColor.secondaryBlue
                      : AppColor.secondaryBlueLight),
            ),
            const SizedBox(
              height: 5,
            ),
            const Divider(
              thickness: 2,
            ),
            MyInformationField(
              controller: labState.descController,
              fieldTitle: 'Nome da Mão de Obra',
              focusNode: labState.descFocusNode,
              onSubmitted: (value) {
                labState.descFocusNode.unfocus();
                FocusScope.of(context).requestFocus(labState.valueFocusNode);
              },
              showAlert: appState.invalidFields
                  .contains(ValidFields.laborOtherCostDesc)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields.remove(ValidFields.laborOtherCostDesc);
                });
              },
            ),
            MyInformationField(
              textInputType: TextInputType.number,
              inputFormatters: [
                CurrencyTextInputFormatter(decimalDigits: 2, symbol: 'R\$')
              ],
              controller: labState.valueController,
              fieldTitle: 'Custo da Mão de Obra (Custo R\$)',
              focusNode: labState.valueFocusNode,
              onSubmitted: (value) {
                labState.valueFocusNode.unfocus();
              },
              showAlert: appState.invalidFields
                  .contains(ValidFields.laborOtherCostValue)
                  ? true
                  : false,
              alertMessage: 'Campo obrigatório',
              onTap: () {
                setState(() {
                  appState.invalidFields
                      .remove(ValidFields.laborOtherCostValue);
                });
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  width: 200,
                  label: 'Adicionar Item +',
                  onPress: () {
                    addItemCost(appState, authState, labState, context);
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            const Divider(
              thickness: 2,
            ),
            labState.otherCustsList.isEmpty
                ? const SizedBox()
                : ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: labState.otherCustsList.length,
                itemBuilder: (context, index) {
                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Item: ${labState.otherCustsList[index].description}',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                              color: appState.darkMode
                                  ? Colors.white
                                  : AppColor.font),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Custo: R\$ ${labState.otherCustsList[index].value}',
                          style: TextStyle(
                              color: appState.darkMode
                                  ? Colors.white
                                  : AppColor.font),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            MyButton(
                              height: 35,
                              label: 'Remover -',
                              backgroundColor: AppColor.redColor,
                              onPress: () {
                                removeOtherCost(appState, authState,
                                    labState, context, index);
                              },
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        const Divider(
                          thickness: 2,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                      ]);
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(
                  label: 'Salvar',
                  onPress: () {
                    saveMachinary(appState, authState, labState, context);
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
          ]);
        });
      });
    });
  }

  void addItemCost(AppProvider appState, AuthProvider authState,
      LaborProvider labState, BuildContext context) {
    appState.invalidFields = [];
    checkFieldLabor(ValidFields.laborOtherCostDesc, appState, labState,
        labState.descController);
    checkFieldLabor(ValidFields.laborOtherCostValue, appState, labState,
        labState.valueController);
    setState(() {});
    if (appState.invalidFields.isEmpty) {
      OtherCostResponse otherCost = OtherCostResponse(
          description: labState.descController.text,
          value: double.parse(labState.valueController.text
              .replaceAll(RegExp(r'[^\d.]+'), '')));
      labState.addOtherCost(otherCost);
      labState.descController.text = '';
      labState.valueController.text = '';
    }
  }

  void removeOtherCost(AppProvider appState, AuthProvider authState,
      LaborProvider labState, BuildContext context, int index) {
    labState.otherCustsList.remove(labState.otherCustsList[index]);
    setState(() {});
  }

  void saveMachinary(AppProvider appState, AuthProvider authState,
      LaborProvider labState, BuildContext context) {
    appState.invalidFields =[];
    LaborResponse payload = LaborResponse();
    checkFieldLabor(ValidFields.commonLaborCost, appState, labState, labState.commonLaborCostController);
    checkFieldLabor(ValidFields.driverCost, appState, labState, labState.driverCostController);
    checkFieldLabor(ValidFields.tractorDriverCost, appState, labState, labState.tractorDriverCostController);
    checkFieldLabor(ValidFields.bleederCost, appState, labState, labState.bleederCostController);
    checkFieldLabor(ValidFields.taxCost, appState, labState, labState.taxCostController);
    checkFieldLabor(ValidFields.laborTechnicalAssistanceCost, appState, labState, labState.technicalAssistanceCostController);
    checkFieldLabor(ValidFields.transportCost, appState, labState, labState.transportCostController);

    setState(() {

    });
    if(appState.invalidFields.isNotEmpty){
      final snackBar = SnackBar(
        dismissDirection: DismissDirection.horizontal,
        backgroundColor: Colors.red.withOpacity(0.9),
        content: const Text(
          "Um ou mais itens obrigatórios não foram preenchidos",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }else{
      showDialog(
          context: context,
          builder: (BuildContext context) {
            dialogContext = context;
            return const DefaultAlert(
              text: "Salvando...",
            );
          },
          useSafeArea: true,
          barrierDismissible: false);
      payload = LaborResponse(
        production:labState.loadedActivities[labState.chosenActivity]!,
        commonLaborCostType: labState.commonLaborCostType == CommonLabor.valorDiaria? 'daily':'monthly',
        commonLaborCost:double.parse(labState.commonLaborCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        laborCostType:labState.laborCostType == CommonLabor.valorDiaria? 'daily':'monthly',
        driverCost: double.parse(labState.driverCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        tractorDriverCost:double.parse(labState.tractorDriverCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        bleederCost:double.parse(labState.bleederCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        taxCost:double.parse(labState.taxCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        technicalAssistanceCost:double.parse(labState.technicalAssistanceCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        transportCost:double.parse(labState.transportCostController.text.replaceAll(RegExp(r'[^\d.]+'),'')),
        otherCost: labState.otherCustsList,
        id:labState.laborIten.id
      );

      LaborService().edit(payload, authState.token).then((value) {
        LaborService().getAll(authState.token).then((value) {
          labState.loadedItens = value;
          ProductiveActivityService().getAllProductive(authState.token).then((value) {
            for(LaborResponse labor in labState.loadedItens) {
              labor.production =
                  value.firstWhere((element) => labor.production!.id ==
                      element.id);
            }
          Navigator.pop(dialogContext);
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return LaborMenuPage();
          }));
        });
      }).onError((error, stackTrace) {
        Navigator.pop(dialogContext);
        final snackBar = SnackBar(
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Colors.red.withOpacity(0.9),
          content: Text(
            error.toString().replaceAll('Exception:', ''),
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 18),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });});
    }
  }
}

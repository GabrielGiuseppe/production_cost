import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/pages/home_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:provider/provider.dart';

import '../../../shared/providers/app_provider.dart';
class TermsPage extends StatelessWidget {
  const TermsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return HomeLayout(children: [
        Container(
          padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
          children: [
          Text(AppStrings.termsPageTitle,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700, color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight),),
          const SizedBox(height: 25,),
          Text(AppStrings.termsPageText,
            style: TextStyle(fontSize: 20, color: appState.darkMode?Colors.white:AppColor.font),),
         const SizedBox(height: 25,),
          MyButton(label: AppStrings.termsPageButton,width: 250,
            onPress: (){
            Navigator.pop(context);
            },
          ),
            const SizedBox(height: 25,),
        ],))
      ]);
    });
  }
}

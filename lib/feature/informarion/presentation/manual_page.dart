import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/pages/home_layout.dart';
import 'package:production_cost/shared/presentation/widgets/my_button.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:provider/provider.dart';

import '../../../shared/providers/app_provider.dart';

class ManualPage extends StatelessWidget {
  const ManualPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return HomeLayout(children: [
        Center(
          child: Container(
            padding: const EdgeInsets.only(left: 15,right: 15),
            child: Column(
              children: [
                Text(
                  AppStrings.manualPageTitle,
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 30,
                      color: appState.darkMode
                          ? AppColor.secondaryBlue
                          : AppColor.secondaryBlueLight),
                ),
                const SizedBox(height: 30,),
                Text(AppStrings.manualPageText,
                  style: TextStyle(

                      fontSize: 20,
                      color: appState.darkMode
                          ? Colors.white
                          : AppColor.font),),
                const SizedBox(height: 30,),
                const MyButton(label: AppStrings.manualPageButton,width: 200,)
              ],
            ),
          ),
        )
      ]);
    });
  }
}

import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/pages/home_layout.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../shared/providers/app_provider.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return HomeLayout(
        children: [
          Center(
              child: Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  AppStrings.contactPageTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w700,
                      color: appState.darkMode ? Colors.white : AppColor.font),
                ),
                const SizedBox(height: 30,),
                Text(
                  AppStrings.contactPageText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                      color: appState.darkMode ? Colors.white : AppColor.font),
                ),
                const SizedBox(height: 20,),
                InkWell(
                  onTap: _launchURL,
                  child: Container(
                    alignment: Alignment.center,
                    width: 200,
                    height: 50,
                    child: Text(
                    AppStrings.contactPageLink,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color:
                            appState.darkMode ? AppColor.secondaryBlueLight : AppColor.secondaryBlueLight,fontWeight: FontWeight.w700, fontSize: 18),
                  ),)
                )
              ],
            ),
          ))
        ],
      );
    });
  }

  _launchURL() async {
    if (await canLaunchUrl(Uri.parse("http://${AppStrings.contactPageLink}"))) {
      await launchUrl(Uri.parse("http://${AppStrings.contactPageLink}"),
          mode: LaunchMode.externalApplication);
    }
  }
}

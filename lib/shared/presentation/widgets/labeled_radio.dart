import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';


class LabeledRadio extends StatelessWidget {
  const LabeledRadio({
    Key? key,
    required this.label,
    required this.padding,
    required this.groupValue,
    required this.value,
    this.labelChild = '',
    required this.onChanged,
  }) : super(key: key);

  final String label;
  final EdgeInsets padding;
  final String? labelChild;
  final CommonLabor? groupValue;
  final CommonLabor value;
  final ValueChanged<CommonLabor> onChanged;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
      builder: (context, appState, child) => InkWell(
        onTap: () {
          if (value != groupValue) {
            onChanged(value);
          }
        },
        child: Padding(
          padding: padding,
          child: Row(
            children: <Widget>[
              Radio(
                  groupValue: groupValue,
                  value: value,
                  onChanged: (CommonLabor? newValue) {
                    onChanged(newValue!);
                  },
                  hoverColor: appState.darkMode ? Colors.white : AppColor.font,
                  fillColor: MaterialStateProperty.resolveWith((states) {
                    if (states.contains(MaterialState.selected)) {
                      return AppColor.secondaryBlue;
                    }
                    return AppColor.secondaryBlue;
                  })),
              Row(
                children: [
                  Text(
                    label,
                    style: TextStyle(
                        color:
                        appState.darkMode ? Colors.white : AppColor.font),
                  ),
                  Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Text(
                      labelChild!,
                      style: TextStyle(
                          fontSize: 8,
                          color:
                          appState.darkMode ? Colors.white : AppColor.font),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
enum CommonLabor { valorDiaria, mensal }

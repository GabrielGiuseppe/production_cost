import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:provider/provider.dart';

class DefaultAlert extends StatelessWidget {
  const DefaultAlert({Key? key, required this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
      builder: (context, appState, child) => AlertDialog(
        backgroundColor: appState.darkMode? AppColor.surfaceDark : Colors.white,
        contentTextStyle: TextStyle(
          color: appState.darkMode? Colors.white : AppColor.font,
          fontSize: 20,),
        content: SizedBox(
          height: 100,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:  [
                Text(text),
                const CircularProgressIndicator(),
              ]),
        ),
      ),
    );
  }
}
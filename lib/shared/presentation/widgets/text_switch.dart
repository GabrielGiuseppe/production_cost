import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';

class TextSwitch extends StatelessWidget {
  const TextSwitch({
    Key? key,
    required this.onChanged,
    required this.label,
    required this.initialValue
  }) : super(key: key);

  final ValueChanged<bool>? onChanged;
  final String label;
  final bool initialValue;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
        builder: (context, value, child) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children:  [
            Text(
                label,
                style: TextStyle(
                    color: value.darkMode ? Colors.white : AppColor.font)
            ),
            Switch(
              value: initialValue,
              onChanged: onChanged,
              activeColor: AppColor.cardLight,
              activeTrackColor: AppColor.secondaryBlueDark,
            )
          ],
        )
    );
  }
}

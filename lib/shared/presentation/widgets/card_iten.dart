import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:provider/provider.dart';

class CardIten extends StatefulWidget {
  const CardIten({Key? key, this.itenTitle = 'Item Title', this.itenSubtitle = 'Iten Subtitle', this.itenDescription = 'Item Description', this.onTap, this.isLonelyInformation = false}) : super(key: key);

  final String itenTitle;
  final String itenSubtitle;
  final String itenDescription;
  final Function()? onTap;
  final bool isLonelyInformation;

  @override
  State<CardIten> createState() => _CardItenState();
}

class _CardItenState extends State<CardIten> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child){
      return InkWell(
        onTap: widget.onTap ,
          child: Container(
          child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: appState.darkMode? AppColor.surfaceDark: Colors.white,
        elevation: 5,
        child: Padding(padding: EdgeInsets.only(left: 15,right: 5, top: 10, bottom: 10) ,child: Row(children: [
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              widget.isLonelyInformation? SizedBox(height: 15,): SizedBox(),
            Text(widget.itenTitle,
            style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font, fontSize: 30, fontWeight: FontWeight.w700),),
              widget.isLonelyInformation? SizedBox(height: 10,): SizedBox(),
              SizedBox(height: 5,),
             widget.isLonelyInformation? const SizedBox():Text(widget.itenSubtitle,
                style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font, fontSize: 15, ),),
              SizedBox(height: 5,),
              widget.isLonelyInformation? const SizedBox():Text(widget.itenDescription,
                style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font, fontSize: 16, ),)
          ],)),
          Icon(Icons.arrow_forward_ios, size: 30,
          color: appState.darkMode?Colors.white: AppColor.font,)
        ],),
      ),)));
    });
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/service/city_service.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';
import 'package:provider/provider.dart';

import '../../providers/app_provider.dart';
import 'default_alert.dart';

class MyDropDownField extends StatefulWidget {
  MyDropDownField(
      {Key? key, required this.valuesList, this.fieldTitle = "Titulo", required this.initialValue, this.update, this.fieldType = FieldType.none})
      : super(key: key);

  final  update;
  final List<String> valuesList;
  final String fieldTitle;
  String initialValue;
  FieldType fieldType;

  @override
  State<MyDropDownField> createState() => _MyDropDownFieldState();
}

class _MyDropDownFieldState extends State<MyDropDownField> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<ActivityProvider>(builder: (context, actState, child) {
        return Consumer<MachinaryProvider>(builder: (context, macState, child) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: const EdgeInsets.only(left: 5), child: Text(
            widget.fieldTitle,
            style: TextStyle(
                fontSize: 18,
                color: appState.darkMode ? Colors.white : AppColor.font,
                fontWeight: FontWeight.w700),
          ),),
          const SizedBox(height: 5,),
          Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.fromBorderSide(BorderSide(
                      color: appState.darkMode ? Colors.white : AppColor
                          .font))),
              child: DropdownButton<String>(
                icon: Icon(Icons.keyboard_arrow_down, size: 30,color: appState.darkMode? Colors.white: AppColor.font,),
                dropdownColor: appState.darkMode? AppColor.surfaceDark:Colors.white,
                isExpanded: true,
                underline: const SizedBox(),
                onChanged: (value){
                  setState(() {
                    widget.initialValue = value!;
                  });
                  setValue(appState,actState,macState, value!, widget.fieldType);
                },
                value: widget.initialValue,
                items: widget.valuesList.map((item) {
                  return DropdownMenuItem<String>(
                    value: item,
                    child: Text(item, style: TextStyle(fontSize: 18,
                        color: appState.darkMode ? Colors.white : AppColor
                            .font),),
                  );
                }).toList(),
              )),
          const SizedBox(height: 25,),
        ],
      );
    });});});
  }

  void setValue(AppProvider appState, ActivityProvider actState,MachinaryProvider macState, String value, FieldType fieldType) {
    switch (fieldType){
      case(FieldType.profission):appState.setAccessProfileValue(value);break;
      case(FieldType.possession):appState.setPossessionConditionValue(value);break;
      case(FieldType.unity):appState.setUnityValue(value);break;
      case(FieldType.uf):appState.setUfValue(value); callCities(appState);break;
      case(FieldType.tecnologies):actState.setTecnologyValue(value);break;
      case(FieldType.mesureUnity):actState.setUnityValue(value);break;
      case(FieldType.fuel):macState.setFuelTypeValue(value);break;
      default:break;
    }
  }

  void callCities(AppProvider appState) {
    late BuildContext dialogContext;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return const DefaultAlert(
            text: "Buscando...",
          );
        },
        useSafeArea: true,
        barrierDismissible: false);
    CityService().getCities(appState.ufValue).then((value) {
      appState.cityList = value;
      Navigator.pop(dialogContext);
    }).onError((error, stackTrace) {
      Navigator.pop(dialogContext);
    });
  }
}

enum FieldType{
  profission, possession, unity, uf, tecnologies, mesureUnity, fuel, none
}

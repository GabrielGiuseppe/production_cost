import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:production_cost/shared/utils/input_masks.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';

class MyInformationField extends StatefulWidget {
  MyInformationField(
      {Key? key,
      this.fieldTitle = "Titulo",
      required this.controller,
      this.inputFormatters,
      this.textInputType = TextInputType.text,
      this.width = 500,
      this.focusNode,
      this.nextFocus,
      this.onSubmitted,
      this.showAlert = false,
      this.alertMessage = '',
      this.onTap,
      this.disabled = false,
      this.textCapitalization = TextCapitalization.words,
      this.obscureText = false,
      this.hasIcon = false,
      this.icon,
      this.iconTap})
      : super(key: key);

  final String fieldTitle;
  final TextInputType textInputType;
  final TextEditingController controller;
  final List<TextInputFormatter>? inputFormatters;
  final double width;
  final FocusNode? focusNode;
  final FocusNode? nextFocus;
  bool showAlert;
  final String alertMessage;
  final Function()? onTap;
  final Function()? iconTap;
  final onSubmitted;
  final bool disabled;
  final TextCapitalization textCapitalization;
  final bool obscureText;
  final bool hasIcon;
  final IconData? icon;

  @override
  State<MyInformationField> createState() => _MyInformationFieldState();
}

class _MyInformationFieldState extends State<MyInformationField> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Container(
          width: widget.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  widget.fieldTitle,
                  style: TextStyle(
                      fontSize: 18,
                      color: appState.darkMode ? Colors.white : AppColor.font,
                      fontWeight: FontWeight.w700),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              widget.hasIcon
                  ? Container(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          border: Border.fromBorderSide(BorderSide(
                              color: appState.darkMode
                                  ? Colors.white
                                  : AppColor.font))),
                      child: Row(children: [
                        Expanded(child: TextField(
                          style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),
                          obscureText: widget.obscureText,
                          enabled: widget.disabled ? false : true,
                          controller: widget.controller,
                          onTap: widget.onTap,
                          focusNode: widget.focusNode ?? FocusNode(),
                          onSubmitted: widget.onSubmitted,
                          keyboardType: widget.textInputType,
                          inputFormatters: widget.inputFormatters ?? [],
                          decoration:
                              const InputDecoration(border: InputBorder.none),
                          cursorColor:
                              appState.darkMode ? Colors.white : AppColor.font,
                          textCapitalization: widget.textCapitalization,
                        ),),
                        InkWell(child: Icon(widget.icon ?? Icons.remove_red_eye,color: appState.darkMode? Colors.white:AppColor.font,),
                        onTap: widget.iconTap,)
                      ]))
                  : Container(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          border: Border.fromBorderSide(BorderSide(
                              color: appState.darkMode
                                  ? Colors.white
                                  : AppColor.font))),
                      child: TextField(
                        style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),
                        obscureText: widget.obscureText,
                        enabled: widget.disabled ? false : true,
                        controller: widget.controller,
                        onTap: widget.onTap,
                        focusNode: widget.focusNode ?? FocusNode(),
                        onSubmitted: widget.onSubmitted,
                        keyboardType: widget.textInputType,
                        inputFormatters: widget.inputFormatters ?? [],
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                        cursorColor:
                            appState.darkMode ? Colors.white : AppColor.font,
                        textCapitalization: widget.textCapitalization,
                      ),
                    ),
              const SizedBox(
                height: 5,
              ),
              widget.showAlert
                  ? Text(
                      widget.alertMessage,
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    )
                  : const SizedBox(),
              const SizedBox(
                height: 25,
              ),
            ],
          ));
    });
  }
}

import 'package:flutter/material.dart';
class BottonMenuButton extends StatelessWidget {
  const BottonMenuButton({
    Key? key,
    this.color = Colors.white,
    required this.icon,
    this.label = 'Label',
    this.onPress,
  }) : super(key: key);

  final Color color;
  final Image icon;
  final String label;
  final Function()? onPress;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        margin: const EdgeInsets.all(8),
        child: Column(children: [
          icon,
          Text(label, style: TextStyle(
              color: color,
            fontWeight: FontWeight.w500,
            fontSize: 14
          ),)
        ],),
      ),
    );
  }
}

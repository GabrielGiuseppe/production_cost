import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';

class MyButton extends StatelessWidget {
  const MyButton(
      {Key? key,
      this.label = 'Button',
      this.backgroundColor = AppColor.secondaryBlueLight,
      this.height = 50,
      this.width = 150,
      this.fontColor = Colors.white,
      this.fontWeight = FontWeight.w500,
      this.fontSize = 20,
      this.onPress,
      this.borderColor = Colors.transparent})
      : super(key: key);

  final String label;
  final Color backgroundColor;
  final Color fontColor;
  final FontWeight fontWeight;
  final double fontSize;
  final double height;
  final double width;
  final GestureTapCallback? onPress;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
          height: height,
          width: width,
        decoration: BoxDecoration(
        color: backgroundColor,
        border: Border.all(color: borderColor,width: 2 ),
        borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Center(
            child: Text(
          label,
          textAlign: TextAlign.center,
          style: TextStyle(color: fontColor, fontWeight: fontWeight, fontSize: fontSize),
        )),
      ),
    );
  }
}

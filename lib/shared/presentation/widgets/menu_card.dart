import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:provider/provider.dart';

import '../../providers/app_provider.dart';

class MenuCard extends StatelessWidget {
  const MenuCard(
      {Key? key, this.cardText = 'Titulo', this.image = 'assets/images/edit_property_dark.png', this.onTap})
      : super(key: key);

  final String cardText;
  final String image;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return InkWell(
        onTap: onTap,
        child: Card(
          color: appState.darkMode?AppColor.surfaceDark:Colors.white,
          elevation: 5,
          child: Container(
            padding: EdgeInsets.all(5),
            height: 125,
            child:Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
             Image.asset(image),
              Text(cardText, style: TextStyle(color: appState.darkMode?AppColor.secondaryBlue:AppColor.secondaryBlueLight,
              fontWeight: FontWeight.w700),)
            ],
          ),
        ),
      ));
    });
  }
}

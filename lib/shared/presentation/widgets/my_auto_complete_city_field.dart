import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';

class MyAutoCompleteCityField extends StatefulWidget {
   MyAutoCompleteCityField({Key? key, this.fieldTitle = 'Titulo' ,this.onSubmitted,this.prefillText})
      : super(key: key);

  final String fieldTitle;
  final Function(String)? onSubmitted;
  final String? prefillText;

  @override
  State<MyAutoCompleteCityField> createState() => _MyAutoCompleteCityFieldState();
}

class _MyAutoCompleteCityFieldState extends State<MyAutoCompleteCityField> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(
                widget.fieldTitle,
                style: TextStyle(
                    fontSize: 18,
                    color: appState.darkMode ? Colors.white : AppColor.font,
                    fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.fromBorderSide(BorderSide(
                      color:
                          appState.darkMode ? Colors.white : AppColor.font))),
              child: Autocomplete(
                onSelected: (String selected){
                  appState.setCityValue(selected);
                },
                optionsBuilder: (TextEditingValue textEditValue) {
                  if (textEditValue.text.isEmpty) {
                    return const Iterable<String>.empty();
                  } else {
                    return appState.cityList.cityModelList!.where((city) => city
                        .toLowerCase()
                        .contains(textEditValue.text.toLowerCase()));
                  }
                },
                fieldViewBuilder:
                    (context, controller, focusNode, onEdittingComplete) {
                  controller.text = widget.prefillText??'';
                  return TextField(
                    style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),
                    onSubmitted: widget.onSubmitted,
                    controller: controller,
                    focusNode: focusNode,
                    onEditingComplete: onEdittingComplete,
                    cursorColor:
                        appState.darkMode ? Colors.white : AppColor.font,
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(border: InputBorder.none),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 25,
            )
          ],
        ),
      );
    });
  }
}

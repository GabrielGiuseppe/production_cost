import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';

class MyDatePickerField extends StatefulWidget {
  const MyDatePickerField(
      {Key? key, this.focusNode, this.nextNode, this.fieldTitle = 'Titulo',
     required this.controller,
      this.keyboardType,
      this.inputFormatters,
      this.onPress,
      this.onSubmitted,
      this.showAlert = false,
      this.alertMessage = '',
      this.onTap})
      : super(key: key);

  final FocusNode? focusNode;
  final FocusNode? nextNode;
  final String fieldTitle;
  final TextEditingController controller;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final Function()? onPress;
  final Function()? onTap;
  final Function(String)? onSubmitted;
  final bool showAlert;
  final String alertMessage;

  @override
  State<MyDatePickerField> createState() => _MyDatePickerFieldState();
}

class _MyDatePickerFieldState extends State<MyDatePickerField> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Padding(padding: const EdgeInsets.only(left: 5), child: Text(
            widget.fieldTitle,
            style: TextStyle(
                fontSize: 18,
                color: appState.darkMode ? Colors.white : AppColor.font,
                fontWeight: FontWeight.w700),
          ),),
          TextFormField(
            onTap: widget.onTap,
            onFieldSubmitted: widget.onSubmitted,
            controller: widget.controller,
            keyboardType: widget.keyboardType,
            inputFormatters: widget.inputFormatters,
            style: TextStyle(color: appState.darkMode ? Colors.white : AppColor.font),
            decoration: InputDecoration(
              suffixIcon:IconButton(
                onPressed: widget.onPress,
                icon: const Icon(Icons.date_range_sharp),
                color: appState.darkMode ? Colors.white : Colors.black,
              ),
              contentPadding: const EdgeInsets.all(15),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide:  BorderSide(
                      width: 1.25, color: appState.darkMode? Colors.white:AppColor.font)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide:  BorderSide(
                      width: 1.25, color: appState.darkMode? Colors.white:AppColor.font)),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide:  BorderSide(
                      width: 1.25, color: appState.darkMode? Colors.white:AppColor.font)),
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide:  BorderSide(
                      width: 1.25, color:appState.darkMode? Colors.white:AppColor.font)),
            ),
          ),
            const SizedBox(height: 5,),
            widget.showAlert? Text(widget.alertMessage, style: TextStyle(color: Colors.red, fontSize: 12),):const SizedBox(),
          const SizedBox(height: 25,)
        ],),
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/utils/app_strings.dart';
import 'package:provider/provider.dart';

import '../../utils/bottom_menu_funtions.dart';
import 'botton_menu_button.dart';

class BottonMenu extends StatelessWidget {
  const BottonMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
        builder: (context, appState, child) => Consumer<AuthProvider>(
            builder: (context, authState, child) => Container(
                  padding: const EdgeInsets.all(5),
                  height: 75,
                  color: appState.darkMode
                      ? AppColor.bottomMenuDark
                      : AppColor.bottomMenuLight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        'assets/images/logo.png',
                        width: 150,
                      ),
                      Expanded(
                        child: Switch(
                            activeColor: Colors.white,
                            activeTrackColor: AppColor.secondaryBlueDark,
                            inactiveTrackColor: AppColor.secondaryBlueLight,
                            value: appState.darkMode,
                            onChanged: (bool newValue) {
                              appState.setDarkMode(newValue);
                            }),
                      ),
                      BottonMenuButton(
                        onPress: () {
                          BMFunctions.homeButton(context,appState,authState);
                        },
                        color: appState.darkMode
                            ? AppColor.iconDark
                            : AppColor.secondaryBlueLight,
                        label: AppStrings.homeButtonLabel,
                        icon: appState.darkMode
                            ? Image.asset('assets/images/home_icon_dark.png')
                            : Image.asset('assets/images/home_icon.png'),
                      ),
                      BottonMenuButton(
                        onPress: (){
                          BMFunctions.contactPage(context, appState, authState);
                        },
                        color: appState.darkMode
                            ? AppColor.iconDark
                            : AppColor.secondaryBlueLight,
                        label: AppStrings.contactButtonLabel,
                        icon: appState.darkMode
                            ? Image.asset('assets/images/contact_icon_dark.png')
                            : Image.asset('assets/images/contact_icon.png'),
                      ),
                      BottonMenuButton(
                        onPress: (){
                          BMFunctions.manualPage(context, appState, authState);
                        },
                        color: appState.darkMode
                            ? AppColor.iconDark
                            : AppColor.secondaryBlueLight,
                        label: AppStrings.manualButtonLabel,
                        icon: appState.darkMode
                            ? Image.asset('assets/images/manual_icon_dark.png')
                            : Image.asset('assets/images/manual_icon.png'),
                      )
                    ],
                  ),
                )));
  }
}

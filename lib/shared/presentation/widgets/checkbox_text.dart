import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';

class CheckboxText extends StatelessWidget {
  const CheckboxText({
    Key? key,
    required this.onChange,
    required this.label,
    required this.initialValue
  }) : super(key: key);

  final ValueChanged<bool?> onChange;
  final String label;
  final bool initialValue;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
      builder: (context, value, child) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:  [
          Text(label,
              style: TextStyle(
                  color: value.darkMode ? Colors.white : AppColor.font),
          ),
          Checkbox(
              value: initialValue,
              onChanged: onChange,
              activeColor: AppColor.secondaryBlueDark
          ),
        ],
      )
    );
  }
}

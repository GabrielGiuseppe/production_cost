import 'package:flutter/material.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:provider/provider.dart';

import '../../../feature/product-activity/data/model/productive_activity_response.dart';
import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';

class MyAutoCompleteActivityField extends StatefulWidget {
  MyAutoCompleteActivityField({Key? key, this.fieldTitle = 'Titulo' ,this.onSubmitted,this.prefillText,this.focusNode
  })
      : super(key: key);

  final String fieldTitle;
  final Function(String)? onSubmitted;
  final String? prefillText;
  final FocusNode? focusNode;

  @override
  State<MyAutoCompleteActivityField> createState() => _MyAutoCompleteActivityFieldState();
}

class _MyAutoCompleteActivityFieldState extends State<MyAutoCompleteActivityField> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return Consumer<ActivityProvider>(builder: (context, actState, child) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(
                widget.fieldTitle,
                style: TextStyle(
                    fontSize: 18,
                    color: appState.darkMode ? Colors.white : AppColor.font,
                    fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.fromBorderSide(BorderSide(
                      color:
                      appState.darkMode ? Colors.white : AppColor.font))),
              child: Autocomplete(
                onSelected: (String selected){
                  actState.setActivityName(selected);
                },
                optionsBuilder: (TextEditingValue textEditValue) {
                  if (textEditValue.text.isEmpty) {
                    return const Iterable<String>.empty();
                  } else {
                    return actState.activityNames.where((activity) => activity
                        .toLowerCase()
                        .contains(textEditValue.text.toLowerCase()));
                  }
                },
                fieldViewBuilder:
                    (context, controller, focusNode, onEdittingComplete) {
                  controller.text = widget.prefillText??'';

                  return TextField(
                    style: TextStyle(color: appState.darkMode?Colors.white:AppColor.font),
                    onSubmitted: widget.onSubmitted,
                    controller: controller,
                    focusNode: focusNode,
                    onEditingComplete: onEdittingComplete,
                    cursorColor:
                    appState.darkMode ? Colors.white : AppColor.font,
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(border: InputBorder.none),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 25,
            )
          ],
        ),
      );
    });});
  }
}

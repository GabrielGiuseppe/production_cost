import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:production_cost/shared/constants/app_colors.dart';
import 'package:production_cost/shared/presentation/widgets/botton_menu.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:provider/provider.dart';

class HomeLayout extends StatelessWidget {
  const HomeLayout({Key? key, required this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
        return SafeArea(
            child: Scaffold(
                body: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 15),
              width: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    alignment: Alignment.bottomCenter,
                    image: AssetImage(
                      'assets/images/home_bg.png',
                    ),
                    fit: BoxFit.fill),
              ),
              child: Column(children: [
                Image.asset(
                  'assets/images/primary_logo.png',
                  scale: 1.3,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 15),
                  transform: Matrix4.translationValues(0, 1, 0),
                  height: 30,
                  decoration: BoxDecoration(
                      color: appState.darkMode
                          ? AppColor.backgroundDark
                          : Colors.white,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(32),
                          topRight: Radius.circular(32))),
                )
              ]),
            ),
            Expanded(
                child: Container(
              color: appState.darkMode ? AppColor.backgroundDark : Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  children: children,
                ),
              ),
            )),
            isKeyboardVisible ? SizedBox() : BottonMenu()
          ],
        )));
      });
    });
  }
}

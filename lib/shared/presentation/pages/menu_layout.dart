import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:provider/provider.dart';

import '../../constants/app_colors.dart';
import '../../providers/app_provider.dart';
import '../widgets/botton_menu.dart';
class MenuLayout extends StatefulWidget {
  const MenuLayout({Key? key, this.pageTitle = 'Titulo' ,required this.children}) : super(key: key);

 final String pageTitle;
 final List<Widget> children;

  @override
  State<MenuLayout> createState() => _MenuLayoutState();
}

class _MenuLayoutState extends State<MenuLayout> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(builder: (context, appState, child) {
      return KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
        return SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: appState.darkMode
                ? AppColor.backgroundDark
                : AppColor.backgroundLight,
            appBar: AppBar(
              backgroundColor: AppColor.secondaryBlueLight,
              title: Text(
                widget.pageTitle,
                style:
                const TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              centerTitle: true,
            ),
            body: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                        child: Container(
                          padding: const EdgeInsets.all(15),
                          child: Container(
                                padding: const EdgeInsets.all(30),
                                width: double.infinity,
                                child: Column(
                                  children: widget.children,
                                ),
                              )),
                        )),
                isKeyboardVisible?  SizedBox() :  BottonMenu()
              ],
            ),
          ),
        );
      });
    });
  }
}

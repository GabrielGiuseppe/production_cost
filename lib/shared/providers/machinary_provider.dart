import 'package:flutter/cupertino.dart';
import 'package:production_cost/feature/machianry/data/model/machine_response.dart';
import 'package:production_cost/shared/presentation/widgets/my_dropdown_field.dart';

import '../../feature/machianry/data/model/other_cost_response.dart';

class MachinaryProvider extends ChangeNotifier{
  List<MachineResponse> loadedMachines = [];
  MachineResponse machineIten = MachineResponse();
  List<String> machineTypeList = [];
  String machineTypeIten = '';
  List<String> fuelTypeList = ["Biogás", "Biometano", "Diesel", "Etanol", "Gasolina"];
  String fuelTypeValue = '';
  List<OtherCostResponse> otherCostList = [];
  List<FieldType> invalidFields = [];

  TextEditingController machineTypeController = TextEditingController(text: '');
  FocusNode machineTypeFocusNode = FocusNode();
  TextEditingController yearsOfUseController = TextEditingController();
  FocusNode yearsOfUseFocusNode = FocusNode();
  TextEditingController purchaseDateController = TextEditingController();
  FocusNode purchaseDateFocusNode = FocusNode();
  TextEditingController machinePriceController = TextEditingController();
  FocusNode machinePriceFocusNode = FocusNode();
  TextEditingController litersOfFuelController = TextEditingController();
  FocusNode litersOfFuelFocusNode = FocusNode();
  TextEditingController hoursOfUsePerYearController = TextEditingController();
  FocusNode hoursOfUsePerYearFocusNode = FocusNode();
  TextEditingController fuelCapacityCostController = TextEditingController();
  FocusNode fuelCapacityCostFocusNode = FocusNode();
  TextEditingController hourlyCostController = TextEditingController();
  FocusNode hourlyCostFocusNode = FocusNode();
  TextEditingController sumOfOilUsedCostController = TextEditingController();
  FocusNode sumOfOilUsedCostFocusNode = FocusNode();
  TextEditingController sumOfFilterUsedCostController = TextEditingController();
  FocusNode sumOfFilterUsedCostFocusNode = FocusNode();
  TextEditingController literOfAdditivesCostController = TextEditingController();
  FocusNode literOfAdditivesCostFocusNode = FocusNode();
  TextEditingController greaseCostController = TextEditingController();
  FocusNode greaseCostFocusNode = FocusNode();
  TextEditingController descController = TextEditingController();
  FocusNode descTypeFocusNode = FocusNode();
  TextEditingController valueController = TextEditingController();
  FocusNode valueFocusNode = FocusNode();

  void setMachineTypeValue(String selected) {
    machineTypeIten = selected;
    machineTypeController.text = selected;
    notifyListeners();
  }

  void disposeControllers() {
     machineTypeController = TextEditingController(text: '');
     yearsOfUseController = TextEditingController(text: '');
     purchaseDateController = TextEditingController(text: '');
     machinePriceController = TextEditingController(text: '');
     litersOfFuelController = TextEditingController(text: '');
     hoursOfUsePerYearController = TextEditingController(text: '');
     fuelCapacityCostController = TextEditingController(text: '');
     hourlyCostController = TextEditingController(text: '');
     sumOfOilUsedCostController = TextEditingController(text: '');
     sumOfFilterUsedCostController = TextEditingController(text: '');
     literOfAdditivesCostController = TextEditingController(text: '');
     greaseCostController = TextEditingController(text: '');
     descController = TextEditingController(text: '');
     valueController = TextEditingController(text: '');
    notifyListeners();
  }

  void setFuelTypeValue(String value) {
    fuelTypeValue = value;
    notifyListeners();
  }

  void addOtherCost(OtherCostResponse otherCost) {
    this.otherCostList.add(otherCost);
    notifyListeners();
  }

  void removeOtherCust(OtherCostResponse otherCost) {
    this.otherCostList.remove(otherCost);
    notifyListeners();
  }

}
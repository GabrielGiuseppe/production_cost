import 'package:flutter/material.dart';
import 'package:production_cost/feature/auth/data/model/property.dart';

class PropertyProvider extends ChangeNotifier{

    Property property = Property();


    TextEditingController professionController = TextEditingController(text: '');
    FocusNode professionNode = FocusNode();
    TextEditingController nameController = TextEditingController(text: '');
    FocusNode nameNode = FocusNode();
    TextEditingController cpfController = TextEditingController(text: '');
    FocusNode cpfNode = FocusNode();
    TextEditingController dateController = TextEditingController(text: '');
    FocusNode dateNode = FocusNode();
    TextEditingController dddController = TextEditingController(text: '');
    FocusNode dddNode = FocusNode();
    TextEditingController phoneController = TextEditingController(text: '');
    FocusNode phoneNode = FocusNode();
    TextEditingController possessionController = TextEditingController(text: '');
    FocusNode possessionNode = FocusNode();
    TextEditingController propertyNameController =
    TextEditingController(text: '');
    FocusNode propertyNameNode = FocusNode();
    TextEditingController cnpjController = TextEditingController(text: '');
    FocusNode cnpjNode = FocusNode();
    TextEditingController propertieSizeController =
    TextEditingController(text: '');
    FocusNode propertieSizeNode = FocusNode();
    TextEditingController cityController = TextEditingController(text: '');
    FocusNode cityNode = FocusNode();
    FocusNode latNode = FocusNode();
    FocusNode longNode = FocusNode();
    TextEditingController plusCodeController = TextEditingController(text: '');
    FocusNode plusCodeNode = FocusNode();
    TextEditingController emailController = TextEditingController(text: '');
    FocusNode emailNode = FocusNode();
    bool isBiometric = false;




    setProperty(Property property){
      this.property = property;
      notifyListeners();
    }
}
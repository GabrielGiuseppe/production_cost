import 'package:flutter/cupertino.dart';

import '../../feature/product-activity/data/model/productive_activity_response.dart';

class ActivityProvider extends ChangeNotifier{

  List<ProductiveActivityResponse> loadedActivities = [];
  ProductiveActivityResponse activityIten = ProductiveActivityResponse();
  List<String> activityNames =[];
  Map<String, ProductiveActivityNameResponse> namesMap = {};
  ProductiveActivityNameResponse chosenActivityName = ProductiveActivityNameResponse();
  List<String> tecnologies = ["Mecânica", "Semimecânica", "Manual", "Plantio Direto", "Sistema irrigado", "Espaçamento Adensado", "Espaçamento Convencional"];
  String chosenTecnology = '';
  List<String> unityList = ["Kg","Caixa", "Saca", "Tonelada", "m3"];
  String chosenUnity = '';
  bool isSubjectiveUnity = false;


  TextEditingController activityNameController = TextEditingController();
  FocusNode activityNameNode = FocusNode();
   TextEditingController technologyController = TextEditingController();
  FocusNode technologyNode = FocusNode();
   TextEditingController pmuController = TextEditingController();
  FocusNode pmuNode = FocusNode();
   TextEditingController sizeController = TextEditingController();
  FocusNode sizeNode = FocusNode();
   TextEditingController expectationController = TextEditingController();
  FocusNode expectationNode = FocusNode();
   TextEditingController measurementUnitController = TextEditingController();
  FocusNode measurementUnitNode = FocusNode();
   TextEditingController capacityCodeController = TextEditingController();
  FocusNode capacityCodeNode = FocusNode();
   TextEditingController insuranceCostController = TextEditingController();
  FocusNode insuranceCostNode = FocusNode();
   TextEditingController technicalAssistanceCostController = TextEditingController();
  FocusNode technicalAssistanceCostNode = FocusNode();
   TextEditingController yearlyInterestCostController = TextEditingController();
  FocusNode yearlyInterestCostNode = FocusNode();
  TextEditingController monthUnderInterestCostController = TextEditingController();
  FocusNode monthUnderInterestInterestCostNode = FocusNode();
  TextEditingController taxCESSRCostController = TextEditingController();
  FocusNode taxCESSRInterestCostNode = FocusNode();
   TextEditingController deprecationInfoController = TextEditingController();
  FocusNode deprecationInfoNode = FocusNode();
   TextEditingController yearlyOperationCostOfTrainingController = TextEditingController();
  FocusNode yearlyOperationCostOfTrainingNode = FocusNode();
   TextEditingController productiveLifeCostController = TextEditingController();
  FocusNode productiveLifeCostNode = FocusNode();

  setActivityIten(ProductiveActivityResponse iten){}

  void setActivityName(String selected) {
    this.chosenActivityName = this.namesMap[selected]!;
    activityNameController.text = selected;
    notifyListeners();
  }
  void setLoadedActivities(List<ProductiveActivityResponse> list){
    loadedActivities = list;
    notifyListeners();
  }
  void setActivitNameList(List<ProductiveActivityNameResponse> list){
    namesMap = {};
    activityNames =[];
    for(ProductiveActivityNameResponse name in list){
      activityNames.add(name.description!);
      namesMap[name.description!] = name;
    }
  }

  void setTecnologyValue(String value) {
    chosenTecnology = value;
    notifyListeners();
  }

  void setUnityValue(String value) {
    chosenUnity = value;
    if(value == 'Caixa' || value == 'Saca'){
      isSubjectiveUnity = true;
    }else{
      isSubjectiveUnity = false;
    }
    notifyListeners();
  }

  void disposeControllers() {
    activityNameController.text = '';
    technologyController.text = '';
    pmuController.text = '';
     sizeController.text = '';
    expectationController.text = '';
    measurementUnitController.text = '';
    capacityCodeController.text = '';
    insuranceCostController.text = '';
    technicalAssistanceCostController.text = '';
    yearlyInterestCostController.text = '';
    deprecationInfoController.text = '';
     yearlyOperationCostOfTrainingController.text = '';
    productiveLifeCostController.text = '';
  }
}
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:production_cost/feature/auth/data/model/city_model.dart';

import '../utils/register_fild_validator.dart';

class AppProvider extends ChangeNotifier {
  double _deviceHeight = 0;
  double _deviceWidth = 0;
  bool _darkMode = false;

  //page location
  bool isOnHome = true;
  bool isOnContact = false;
  bool isOnManual = false;

  bool get darkMode => _darkMode;

  double get deviceHeight => _deviceHeight;

  double get deviceWidth => _deviceWidth;

  //Register information
  String accessProfileValue = '';
  List<String> accessProfileList = ['Proprietário','Arrendatário','Estudante','Profissional'];
  bool isProfissional = false;
  String possessionConditionValue = '';
  List<String> possessionConditionlist = ['Própria', 'Arrendamento', 'Locatário', 'Meeiro', 'Parceria','Outros'];
  bool isOthers = false;
  String unityValue = '';
  List<String> unityList = ['hectares', 'alqueires paulistas',  'alqueires mineiros',  'alqueires goiano',  'alqueires baianos',  'alqueires do norte'];
  String ufValue = '';
  List<String> ufList = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO'];
  String cityValue = '';
  CityModelList cityList = CityModelList();
  List<ValidFields> invalidFields = [];
  double lat = 0;
  double long = 0;
  TextEditingController longController = TextEditingController(text: '');
  TextEditingController latController = TextEditingController(text: '');



  setAccessProfileValue(String value){
    accessProfileValue = value;
    if(value == 'Profissional'){
      isProfissional = true;
    }else{
      isProfissional = false;
    }
    notifyListeners();
  }

  setPossessionConditionValue(String value){
    possessionConditionValue = value;
    if(value == 'Outros'){
      isOthers = true;
    }else{
      isOthers = false;
    }
    notifyListeners();
  }
  setUfValue(String value){
    ufValue = value;
    notifyListeners();
  }
  setUnityValue(String value){
    unityValue = value;
    notifyListeners();
  }
  setCityValue(String value){
    cityValue = value;
    notifyListeners();
  }

    setDarkMode(bool value) {
    _darkMode = value;
    notifyListeners();
  }

  setDeviceWidth(double value) {
    _deviceWidth = value;
  }

  setDeviceHeight(double value) {
    _deviceHeight = value;
  }

  Future<Position> getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }
}

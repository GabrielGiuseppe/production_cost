import 'package:flutter/cupertino.dart';

import '../../feature/labor/data/model/labor_response.dart';
import '../../feature/machianry/data/model/other_cost_response.dart';
import '../../feature/product-activity/data/model/productive_activity_response.dart';
import '../presentation/widgets/labeled_radio.dart';
import '../presentation/widgets/my_dropdown_field.dart';

class LaborProvider extends ChangeNotifier{

  List<LaborResponse> loadedItens = [];
  LaborResponse laborIten = LaborResponse();
  CommonLabor? commonLaborCostType = CommonLabor.valorDiaria;
  CommonLabor? laborCostType = CommonLabor.valorDiaria;
  Map<String, ProductiveActivityResponse> loadedActivities = {};
  List<String> allActivities = [];
  String chosenActivity = '';
  List<OtherCostResponse> otherCustsList = [];

  TextEditingController activityNameController =TextEditingController();
  FocusNode activityNameFocusNode = FocusNode();
  TextEditingController commonLaborCostController = TextEditingController();
  FocusNode commonLaborCostFocusNode = FocusNode();
  TextEditingController driverCostController = TextEditingController();
  FocusNode driverCostFocusNode = FocusNode();
  TextEditingController tractorDriverCostController = TextEditingController();
  FocusNode tractorDriverCostFocusNode = FocusNode();
  TextEditingController bleederCostController = TextEditingController();
  FocusNode bleederCostFocusNode = FocusNode();
  TextEditingController taxCostController = TextEditingController();
  FocusNode taxCostFocusNode = FocusNode();
  TextEditingController technicalAssistanceCostController = TextEditingController();
  FocusNode technicalAssistanceCostFocusNode = FocusNode();
  TextEditingController transportCostController = TextEditingController();
  FocusNode transportCostFocusNode = FocusNode();
  TextEditingController descController = TextEditingController();
  FocusNode descFocusNode = FocusNode();
  TextEditingController valueController = TextEditingController();
  FocusNode valueFocusNode = FocusNode();

  void disposeControllers() {
    allActivities = [];
    otherCustsList = [];
    chosenActivity = '';
    loadedActivities ={};
    activityNameController = TextEditingController();
    commonLaborCostController = TextEditingController();
    driverCostController = TextEditingController();
    tractorDriverCostController = TextEditingController();
    bleederCostController = TextEditingController();
    taxCostController = TextEditingController();
    technicalAssistanceCostController = TextEditingController();
    transportCostController = TextEditingController();
    descController = TextEditingController();
    valueController = TextEditingController();
    commonLaborCostType = CommonLabor.valorDiaria;
    laborCostType = CommonLabor.valorDiaria;
  }

  void setActivity(List<ProductiveActivityResponse> value) {
    for(ProductiveActivityResponse activity in value ){
      allActivities.add(activity.activityName!.description!);
      loadedActivities[activity.activityName!.description!] = activity;
    }
    chosenActivity = allActivities.isNotEmpty? allActivities.first: '';
  }

  void addOtherCost(OtherCostResponse otherCost) {
    otherCustsList.add(otherCost);
    notifyListeners();
  }


}
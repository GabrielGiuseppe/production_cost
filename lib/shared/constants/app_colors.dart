import 'dart:ui';

class AppColor{
  static const Color secondaryBlueLight = Color.fromRGBO(8, 81, 151, 1);
  static const Color secondaryBlueDark = Color.fromRGBO(10, 44, 77, 1);
  static const Color secondaryBlue = Color.fromRGBO(27, 136, 241, 1);
  static const Color font = Color.fromRGBO(27, 27, 27, 1);
  static const Color bottomMenuLight = Color.fromRGBO(174, 187, 187, 1);
  static const Color bottomMenuDark = Color.fromRGBO(71, 82, 94, 1);
  static const Color iconDark = Color.fromRGBO(174, 187, 187, 1);
  static const Color backgroundLight = Color.fromRGBO(234, 240, 240, 1);
  static const Color backgroundDark = Color.fromRGBO(37, 44, 51, 1);
  static const Color surfaceDark = Color.fromRGBO(51, 62, 73, 1);
  static const Color lightThemeInsert = Color.fromRGBO(124, 133, 133, 1);
  static const Color cardLight = Color.fromRGBO(250, 253, 253, 1);
  static const Color shadowButton = Color.fromRGBO(0, 4, 4, 0);
  static const Color redColor = Color.fromRGBO(215, 55, 55, 1);
}
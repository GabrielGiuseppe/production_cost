class AppStrings{

  // App title
  static const String appTitle = 'Cálculo IEA do Custo de Produção';

  //botton menu labels
  static const String homeButtonLabel = 'Início';
  static const String contactButtonLabel = 'Contato';
  static const String manualButtonLabel = 'Manual';

  //home Button labels
  static const String registerButtonLabel = 'Cadastrar';
  static const String logInShortLabel = 'Entrar';
  static const String logInLongLabel = 'Entrar no Sistema';

  //home page information
  static const String objectiveTitle = 'Objetivo';
  static const String objectiveText = 'Este aplicativo tem o propósito de fornecer ao produtor rural uma ferramenta de cálculo do custo de produção de sua atividade, utilizando-se a metodologia de custo operacional desenvolvida pelo Instituto de Economia Agrícola.';
  static const String participateTitle = 'Como participar';
  static const String participateText = 'É necessário realizar seu cadastro no aplicativo ou no site, e informar os dados referentes a sua atividade agrícola (operações realizadas, uso de mão de obra, máquinas e implementos), material, insumos e produtos com as quantidades a serem utilizados (inseticida, herbicida, adubo e outros). Depois de criados o login e a senha em qualquer uma dessas duas plataformas, poderão ser feitos o cadastro e a inserção dos dados para o cálculo do custo de produção.';

  //Contact page information
  static const String contactPageTitle = 'IEA - Instituto de Economia Agricola';
  static const String contactPageText = 'Praça Ramos de Azevedo, 254 - 3º Andar - Centro 01037-912 - São Paulo - SP';
  static const String contactPageLink = 'iea.agricultura.sp.gov.br';

  //Terms Page
  static const String termsPageTitle = 'Termos de aceite';
  static const String termsPageText = 'Termo de aceite do Sistema de Custo de Produção\n\nAceito fornecer as informações relacionadas a atividadeagrícola para obter seu custo de produção, utilizando oaplicativo ou site apenas para essa finalidade, respeitando a LeiFederal n. 9.609/1998, que dispõe sobre a proteção dapropriedade intelectual, e os artigos n. 184 e n. 325 do CódigoPenal Brasileiro.\n\nTermo de Compromisso do IEA\n\nO Instituto de Economia Agrícola se compromete a empregar asinformações contidas neste aplicativo para fins de pesquisa,respeitando-se a confidencialidade e a individualidade doprodutor de acordo com os Princípios Fundamentais dasEstatísticas Oficiais (ONU/1994), a Lei de Acesso à Informaçãon. 12.527/2011 e a Lei Geral de Proteção de Dados Pessoais nº13.709/2018.';
  static const String termsPageButton = 'Voltar';
  //Manual Page information
  static const String manualPageTitle = 'Manual de Utilização';
  static const String manualPageText = 'Além das informações sobre ametodologia desenvolvida pelo Instituto de Economia Agrícola descrita de forma simplificada (disponível naíntegra no site), este manual ajuda ousuário no preenchimento dos camposdisponíveis neste aplicativo para aobtenção do custo de produção.';
  static const String manualPageButton = 'Baixa Manual';

  //Login Page
  static const String forgetPassword = 'Esqueci minha senha';
  static const String loginButton = 'Entrar no Sistema';

  //host
  static const String host = 'http://82.180.162.140:3000';
}
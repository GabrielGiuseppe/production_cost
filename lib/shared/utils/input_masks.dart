import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class InputMasks{

  static cpfMask(){
    return MaskTextInputFormatter(
      mask: "###.###.###-##",
      filter: {"#" : RegExp('[0-9]')}
    );
  }

  static dateMask(){
    return MaskTextInputFormatter(
        mask: "##/##/####",
        filter: {"#" : RegExp('[0-9]')}
    );
  }

  static cnpjMask(){
    return MaskTextInputFormatter(
        mask: "##.###.###/####-##",
        filter: {"#" : RegExp('[0-9]')}
    );
  }

  static dddMask(){
    return MaskTextInputFormatter(
        mask: "##",
        filter: {"#" : RegExp('[0-9]')}
    );
  }

  static phoneMask(){
    return MaskTextInputFormatter(
        mask: "#####-####",
        filter: {"#" : RegExp('[0-9]')}
    );
  }


}
String maskCnpj(String cnpj){
  String start = cnpj.substring(0,3);
  String middle = cnpj.substring(3,6);
  String prefix = cnpj.substring(6,8);
  String sufix = cnpj.substring(8,12);
  String digit = cnpj.substring(12);
  return '$start.$middle.$prefix/$sufix-$digit';

}
String maskCpf(String value){
  String start = value.substring(0,3);
  String middle = value.substring(3,6);
  String end = value.substring(6,9);
  String digit = value.substring(9);

  return '$start.$middle.$end-$digit';
}

 String unmaskCpf(String cpf){
  return cpf.replaceAll('.', '').replaceAll('-', '');
}
String unmaskCnpj(String cpf){
  return cpf.replaceAll('.', '').replaceAll('/', '').replaceAll('-', '');
}
String unmaskPhone(String phone){
  return phone.replaceAll('-', '');
}

String dateMask(String date){
  date = date.replaceAll('/', '');
  String day = date.substring(0,2);
  String month = date.substring(2,4);
  String year = date.substring(4,8);
  print(date);
  return '$year-$month-$day';
}

String dateUnmask(String date){
  date = date.replaceAll('-', '');
  String day = date.substring(6,8);
  String month = date.substring(4,6);
  String year = date.substring(0,4);
  print(date);
  return '$day/$month/$year';
}
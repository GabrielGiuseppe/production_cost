import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:production_cost/feature/dashboard_page.dart';
import 'package:production_cost/feature/informarion/presentation/contact_page.dart';
import 'package:production_cost/feature/informarion/presentation/manual_page.dart';
import 'package:production_cost/feature/main_page.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';

class BMFunctions{
  BMFunctions.homeButton( BuildContext context, AppProvider appState, AuthProvider authState){
    if(!appState.isOnHome){
      if(!authState.isLoggedIn){
        notifyAppstatePagePlace(appState, HomePageType.HOME);
        returnMainPage(context);
      }else{
        notifyAppstatePagePlace(appState, HomePageType.HOME);
        returnDashboardPage(context);
      }
    }else{}
  }
  
  static void contactPage(BuildContext context, AppProvider appState, AuthProvider authState) {
    if(!appState.isOnContact){
      notifyAppstatePagePlace(appState, HomePageType.CONTACT);
      Navigator.push(context, MaterialPageRoute(builder: (context){return const ContactPage();}));
    }else{}
  }

  static void manualPage(BuildContext context, AppProvider appState, AuthProvider authState) {
    if(!appState.isOnManual){
      notifyAppstatePagePlace(appState, HomePageType.MANUAL);
      Navigator.push(context, MaterialPageRoute(builder: (context){return const ManualPage();}));
    }else{}
  }

  //Return main pages
  void returnDashboardPage(BuildContext context) {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context){
      return const DashboradPage();
    }), (route) => false);
  }

  void returnMainPage(BuildContext context) {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context){
      return const MainPage();
    }), (route) => false);
  }

  static void notifyAppstatePagePlace(AppProvider appState, HomePageType type) {
    switch (type){
      case HomePageType.HOME: appState.isOnHome = true ; appState.isOnContact = false; appState.isOnManual = false; break;
      case HomePageType.CONTACT: appState.isOnHome = false ; appState.isOnContact = true; appState.isOnManual = false; break;
      case HomePageType.MANUAL: appState.isOnHome = false ; appState.isOnContact = false; appState.isOnManual = true; break;
    }
  }
}

enum HomePageType{
  HOME,CONTACT,MANUAL
}
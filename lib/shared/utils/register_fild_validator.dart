import 'package:flutter/cupertino.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';

import '../providers/app_provider.dart';
import '../providers/labor_provider.dart';
import 'input_masks.dart';

checkField(
  ValidFields field,
  AppProvider appState,
  TextEditingController controller,
) {
  switch (field) {
    case ValidFields.profession:
      return appState.isProfissional
          ? empty(controller)
              ? appState.invalidFields.add(field)
              : controller.text
          : null;
      break;
    case ValidFields.fullName:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.cpf:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : unmaskCpf(controller.text);
      break;
    case ValidFields.birthDate:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.phoneCode:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : unmaskPhone(controller.text);
      break;
    case ValidFields.phone:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : unmaskPhone(controller.text);
      break;
    case ValidFields.possessionConditionDesc:
      return appState.isOthers
          ? empty(controller)
              ? appState.invalidFields.add(field)
              : controller.text
          : null;
      break;
    case ValidFields.propertyName:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.cnpj:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : unmaskCnpj(controller.text);
      break;
    case ValidFields.area:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.city:
      return emptyValue(appState.cityValue)
          ? appState.invalidFields.add(ValidFields.city)
          : appState.cityValue;
      break;
    case ValidFields.latitude:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.longitude:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.email:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.password:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
    case ValidFields.confirmPassword:
      return empty(controller)
          ? appState.invalidFields.add(field)
          : controller.text;
      break;
  }
}

checkFieldActivity(ValidFields value, AppProvider appState,
    ActivityProvider actState, TextEditingController controller) {
  switch (value) {
    case ValidFields.activityName:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : actState.chosenActivityName;
      break;
    case ValidFields.size:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.expectation:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.measurementUnit:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.capacity:
      return actState.isSubjectiveUnity?
      empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text:null;
      break;
    case ValidFields.insuranceCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.technicalAssistanceCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.yearlyInterestCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.monthsUnderInterest:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.taxCESSR:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.yearlyOperationCostOfTraining:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
    case ValidFields.productiveLifeCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;
      break;
  }
}

checkFieldMachinary(ValidFields value, AppProvider appState,
    MachinaryProvider machinaryProvider, TextEditingController controller) {
  switch(value){
    case ValidFields.machineName:break;
    case ValidFields.yearsOfUse:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.purchaseDate:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.machinePrice:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.hoursOfUsePerYear:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.fuelType:break;
    case ValidFields.litersOfFuel:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.fuelCapacityCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.hourlyCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.sumOfOilUsedCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.sumOfFilterUsedCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.literOfAdditivesCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.greaseCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.otherCostDesc:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
    case ValidFields.otherCostValue:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
  }
}

checkFieldLabor(ValidFields value, AppProvider appState,
    LaborProvider labState, TextEditingController controller) {
  switch(value) {
    case ValidFields.laborProduction:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.commonLaborCostType:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.commonLaborCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.laborCostType:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.driverCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.tractorDriverCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.bleederCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.taxCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.laborTechnicalAssistanceCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.transportCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.laborOtherCost:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.laborOtherCostDesc:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
    case ValidFields.laborOtherCostValue:
      return empty(controller)
          ? appState.invalidFields.add(value)
          : controller.text;break;
      break;
  }
}

empty(TextEditingController controller) {
  bool result = controller.text == "" ? true : false;
  return result;
}

emptyValue(String value) {
  return value == '' ? true : false;
}

enum ValidFields {
  profession,
  fullName,
  cpf,
  birthDate,
  phoneCode,
  phone,
  possessionConditionDesc,
  propertyName,
  cnpj,
  area,
  city,
  latitude,
  longitude,
  email,
  password,
  confirmPassword,
  activityName,
  size,
  expectation,
  measurementUnit,
  capacity,
  insuranceCost,
  technicalAssistanceCost,
  yearlyInterestCost,
  monthsUnderInterest,
  taxCESSR,
  yearlyOperationCostOfTraining,
  productiveLifeCost,
  machineName,
  yearsOfUse,
  purchaseDate,
  machinePrice,
  hoursOfUsePerYear,
  fuelType,
  litersOfFuel,
  fuelCapacityCost,
  hourlyCost,
  sumOfOilUsedCost,
  sumOfFilterUsedCost,
  literOfAdditivesCost,
  greaseCost,
  otherCostDesc,
  otherCostValue,
 laborProduction,
 commonLaborCostType,
 commonLaborCost,
 laborCostType,
  driverCost,
 tractorDriverCost,
 bleederCost,
 taxCost,
 laborTechnicalAssistanceCost,
 transportCost,
 laborOtherCost,
  laborOtherCostDesc,
  laborOtherCostValue
}

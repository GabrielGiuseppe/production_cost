import 'package:intl/intl.dart';

class StringUtils {

  String convertDateFormatServer(String date) {
    DateTime tempDate = convertStringToDate(date, "dd/MM/yyyy");
    return convertDateToString(tempDate, "yyyy-MM-dd");
  }

  String convertDateFormatMobile(String date) {
    DateTime tempDate = convertStringToDate(date, "yyyy-MM-dd");
    return convertDateToString(tempDate, "dd/MM/yyyy");
  }

  DateTime convertStringToDate(String date, String format) {
     return DateFormat(format).parse(date);
  }

  String convertDateToString(DateTime date, String format) {
    return DateFormat(format).format(DateTime.parse(date.toString()));
  }

}

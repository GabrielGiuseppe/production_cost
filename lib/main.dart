import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:production_cost/feature/main_page.dart';
import 'package:production_cost/shared/providers/activity_provider.dart';
import 'package:production_cost/shared/providers/app_provider.dart';
import 'package:production_cost/shared/providers/auth_provider.dart';
import 'package:production_cost/shared/providers/labor_provider.dart';
import 'package:production_cost/shared/providers/machinary_provider.dart';
import 'package:production_cost/shared/providers/property_provider.dart';
import 'package:provider/provider.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<AppProvider>(
        create: (BuildContext context) => AppProvider(),
      ),
      ChangeNotifierProvider<AuthProvider>(
        create: (BuildContext context) => AuthProvider(),
      ),
      ChangeNotifierProvider<PropertyProvider>(
        create: (BuildContext context) => PropertyProvider(),
      ),
      ChangeNotifierProvider<ActivityProvider>(
        create: (BuildContext context) => ActivityProvider(),
      ),
      ChangeNotifierProvider<MachinaryProvider>(
        create: (BuildContext context) => MachinaryProvider(),
      ),
      ChangeNotifierProvider<LaborProvider>(
        create: (BuildContext context) => LaborProvider(),
      ),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
        builder: (context, appState, child) => const MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: [Locale('pt', 'BR')],
              home: MainPage(),
            ));
  }
}
